﻿namespace AtelLib.ATEL.Decompiler.Interfaces
{
    public interface IConditionalToken
    {
        IToken Condition { get; set; }
    }
}