﻿namespace AtelLib.ATEL.Decompiler.Interfaces
{
    public interface ITokenAnalyzer
    {
        bool HasResult { get; }
        IToken Result { get; }
        bool Analyze();

    }
}