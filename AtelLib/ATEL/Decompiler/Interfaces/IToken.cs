﻿namespace AtelLib.ATEL.Decompiler.Interfaces
{
    public interface IToken
    {
        uint Offset { get; set; }
        uint StartOffset { get; }
        uint EndOffset { get; }
    }
}