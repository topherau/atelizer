﻿using System.Collections.Generic;

namespace AtelLib.ATEL.Decompiler.Interfaces
{
    public interface IBlockToken
    {
        List<IToken> Children { get; set; }
    }
}