﻿namespace AtelLib.ATEL.Decompiler.Interfaces
{
    public interface INamedToken
    {
        string Name { get; set; }
    }
}