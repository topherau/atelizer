﻿using System;
using System.Collections.Generic;
using System.IO;
using AtelLib.ATEL.Database.Enums;
using AtelLib.ATEL.Decompiler.Analyzers;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Disassembler;

namespace AtelLib.ATEL.Decompiler
{
    public class AtelDecompiler : IDisposable

    {
        private FileStream _logStream;
        public AtelDisassembler Disassembler { get; }
        public BlockAnalyzer[] Blocks { get; set; }
        private StreamWriter _logWriter { get; }

        public AtelDecompiler(AtelDisassembler disassembler, GameType game = GameType.FFX)
        {
            Disassembler = disassembler;

            //_logStream = File.Open("log.txt", FileMode.Create, FileAccess.Write, FileShare.Read);
            //_logWriter = new StreamWriter(_logStream) {AutoFlush = true};

            DecompileBlocks();
        }

        public void Log(string text)
        {
            //_logWriter.WriteLine(text);
        }

        private void DecompileBlocks()
        {
            Blocks = new BlockAnalyzer[Disassembler.BlockDescriptors.Count];
            for (var i = 0; i < Disassembler.BlockDescriptors.Count; i++)
                Blocks[i] = new BlockAnalyzer(this, i);
        }

        public void Dispose()
        {
            _logWriter?.Dispose();
        }
    }
}