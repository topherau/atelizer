﻿namespace AtelLib.ATEL.Decompiler.Enums
{
    public enum RegisterType
    {
        I0,
        I1,
        I2,
        I3,
        F0,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        X,
        Y
    }
}