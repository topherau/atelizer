﻿namespace AtelLib.ATEL.Decompiler.Enums
{
    public enum AtelType
    {
        None,
        Integer,
        Float,
        IntPtr,
        FloatPtr
    }
}