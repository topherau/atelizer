﻿namespace AtelLib.ATEL.Decompiler.Enums
{
    public enum ValueType
    {
        String,
        Int,
        Float
    }
}