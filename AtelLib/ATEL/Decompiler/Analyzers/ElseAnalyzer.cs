﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class ElseAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly LabelConstant _startLabel;
        private readonly LabelConstant _endLabel;
        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public ElseAnalyzer(FunctionAnalyzer fa, LabelConstant startLabel, LabelConstant endLabel)
        {
            _fa = fa;
            _startLabel = startLabel;
            _endLabel = endLabel;

            _stack = fa.Stack;
            _instructions = fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeElse();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeElse()
        {
            var instruction = _instructions[_startLabel.Offset];
            var startPosition = _instructions.IndexOf(instruction);
            var endPosition = _instructions.FindPosition(_endLabel.Offset);
            var endInstruction = _instructions[endPosition];

            _fa.Log($"Analyzing else branch from {_startLabel.Offset:X4} to {endInstruction.Offset}");

            var children = new List<IToken>();
            var currentPosition = startPosition;
            for (; currentPosition < endPosition; currentPosition++)
            {
                if (IsAnalyzed(currentPosition))
                    continue;

                var currentInstruction = _instructions[currentPosition];

                // Add labels for current offset
                var labels = _fa.FindLabels(currentInstruction.Offset)
                    .Select(l => new Label(l.Offset, l)).ToList();
                if(labels.Count() != 0)
                    children.AddRange(labels);

                // Analyze instruction
                var analyzer = new OpcodeAnalyzer(_fa, currentInstruction.Offset);
                analyzer.Analyze();
                Commit(analyzer);

                if (!analyzer.HasResult)
                    continue;
                
                children.Add(analyzer.Result);
                switch (analyzer.Result)
                {
                    case Goto gotoToken:
                    case Return returnToken:
                        goto endAnalysis;
                }
            }

            endAnalysis:

            return new Else(_startLabel.Offset, null, children);
        }
    }
}