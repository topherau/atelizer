﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Database;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class CallAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly InstructionList _instructions;
        private readonly uint _offset;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public CallAnalyzer(FunctionAnalyzer fa, uint offset)
        {
            _fa = fa;
            _offset = offset;
            _stack = _fa.Stack;
            _instructions = _fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeCall();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeCall()
        {
            var instruction = _instructions[_offset];
            var position = _instructions.IndexOf(instruction);
            MarkAnalyzed(position);

            _fa.Log($"Analyzing function call at offset {_offset:X4}");

            var function = AtelDatabase.FindFunction(instruction.Operand);
            if (function == null)
            {
                // Function not found in database, return generic function signature
                _fa.Log($"Unknown function call {instruction.Operand:X4} at offset {_offset:X4}");
                return new Call(_offset, $"unknown_{instruction.Operand:X4}");
            }

            _fa.Log($"Function at offset {_offset:X4} is \"{function.Name}\"");

            var args = new List<IToken>();
            if (function.Arguments.Count != 0)
            {
                // Check if there are enough arguments on the stack
                if (_stack.Count < function.Arguments.Count)
                {
                    // Not enough arguments on stack for function call
                    _fa.Log(
                        $"Unable to unstack arguments for \"{function.Name}\": Function has {function.Arguments.Count} arguments but only {_stack.Count} values are on the stack.");
                    return new Call(_offset, function.Name);
                }

                _fa.Log($"Unstacking {function.Arguments.Count} arguments for \"{function.Name}\"");
                for (var i = 0; i < function.Arguments.Count; i++)
                {
                    var arg = _stack.ElementAt(_stack.Count - i - 1);
                    _fa.Log($"Argument {function.Arguments.Count - i}: \"{arg}\"");
                    args.Insert(0, arg);
                }
            }

            _fa.Log($"Analyzed call is: {function.Name}({string.Join(", ", args)})");
            return new Call(_offset, function, args);
        }
    }
}