﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class WhileAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        public List<IToken> AdjustedResult { get; private set; }

        private readonly FunctionAnalyzer _fa;
        private readonly AnalyzeMode _mode;
        private readonly uint _startOffset;
        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public WhileAnalyzer(FunctionAnalyzer fa, uint startOffset, AnalyzeMode mode = AnalyzeMode.WithConditional)
        {
            _fa = fa;
            _startOffset = startOffset;
            _mode = mode;

            _stack = _fa.Stack;
            _instructions = _fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeWhile();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeWhile()
        {
            switch (_mode)
            {
                case AnalyzeMode.WithConditional:
                    return AnalyzeWithConditional();
                case AnalyzeMode.WithoutConditional:
                    return AnalyzeWithoutConditional();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IToken AnalyzeWithConditional()
        {
            return null;
        }

        private IToken AnalyzeWithoutConditional()
        {
            return null;
        }

        public enum AnalyzeMode
        {
            WithConditional,
            WithoutConditional
        }
    }
}