﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Disassembler.Classes;
using AtelLib.ATEL.Disassembler.Classes.Constants;
using AtelLib.ATEL.Disassembler.Enums;
using Instruction = AtelLib.ATEL.Decompiler.Classes.Instruction;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class FunctionAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        public int FunctionId { get; }
        public BlockAnalyzer BlockAnalyzer { get; }
        public Block Block { get; }

        public FunctionConstant Function { get; set; }
        public InstructionList Instructions { get; }

        public Stack<IToken> Stack { get; private set; }
        public List<IToken> Tokens { get; private set; }

        [DebuggerStepThrough]
        public FunctionAnalyzer(BlockAnalyzer blockAnalyzer, int functionId)
        {
            BlockAnalyzer = blockAnalyzer;
            FunctionId = functionId;

            Block = BlockAnalyzer.Block;
            Function = Block.Functions[functionId];

            Instructions = new InstructionList(Function.Instructions);
            Analyze();
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Log($"Analyzing function {FunctionId}...");

            Stack = new Stack<IToken>();
            Tokens = new List<IToken>();

            if (FunctionId == 0)
            {
                // Add tokens for constructor
                var constructor = new ConstructorAnalyzer(this);
                var tokens = constructor.AnalyzeConstructor();
                if (tokens.Count != 0)
                    Tokens.AddRange(tokens);
            }

            var currentPosition = 0;

            for (; currentPosition < Instructions.Count; currentPosition++)
            {
                if (IsAnalyzed(currentPosition))
                    continue;

                var currentInstruction = Instructions[currentPosition];

                // Add labels for current offset
                var labels = FindLabels(currentInstruction.Offset)
                    .Select(l => new Label(l.Offset, l));
                Tokens.AddRange(labels);

                var analyzer = new OpcodeAnalyzer(this, currentInstruction.Offset);
                analyzer.Analyze();
                Commit(analyzer);

                if (!analyzer.HasResult)
                    continue;

                Tokens.Add(analyzer.Result);
            }

            Log($"Reached the end of function {FunctionId}.");

            Result = new Function(Instructions[0].Offset, $"function_{FunctionId}", Tokens);
            HasResult = Result != null;
            return HasResult;
        }

        [DebuggerStepThrough]
        public void Log(string text)
        {
            BlockAnalyzer.Decompiler.Log(text);
            Console.WriteLine(text);
        }

        /// <summary>
        ///     Search for instructions that jump to the specified label.
        /// </summary>
        /// <param name="label">The destination label to search for.</param>
        /// <returns>A list of instructions that jump to the specified label.</returns>
        [DebuggerStepThrough]
        public List<Instruction> FindJumpsTo(LabelConstant label)
        {
            var labelIndex = Block.Labels.IndexOf(label);
            var instructions = new List<Instruction>();

            foreach (var instruction in Instructions)
            {
                switch (instruction.Opcode)
                {
                    case OpcodeType.JZ:
                    case OpcodeType.JNZ:
                    case OpcodeType.JMP:
                    case OpcodeType.GOTO:
                        // Continue if jump destination is not specified label
                        if (instruction.Operand != labelIndex)
                            continue;
                        break; // Break if jump destination matches
                    default:
                        // Continue if instruction is not jump
                        continue;
                }

                // Add found instruction to the list
                instructions.Add(instruction);
            }

            return instructions;
        }

        /// <summary>
        ///     Search for instructions that jump to the specified offset.
        /// </summary>
        /// <param name="offset">The destination offset to search for.</param>
        /// <returns>A list of instructions that jump to the specified offset.</returns>
        [DebuggerStepThrough]
        public List<Instruction> FindJumpsTo(uint offset)
        {
            var instructions = new List<Instruction>();

            // Find index of all labels that point to the target offset
            var labels = Block.Labels.Where(l => l.Offset == offset).Select(l => Block.Labels.IndexOf(l)).ToList();
            if (labels.Count == 0)
                // No labels at target offset, therefore no jumps to that offset
                return instructions;

            foreach (var instruction in Instructions)
            {
                switch (instruction.Opcode)
                {
                    case OpcodeType.JZ:
                    case OpcodeType.JNZ:
                    case OpcodeType.JMP:
                    case OpcodeType.GOTO:
                        // Continue if jump destination is not specified label
                        if (!labels.Contains(instruction.Operand))
                            continue;
                        break; // Break if jump destination matches
                    default:
                        // Continue if instruction is not jump
                        continue;
                }

                // Add found instruction to the list
                instructions.Add(instruction);
            }

            return instructions;
        }

        /// <summary>
        ///     Search for instructions that match a specific opcode.
        /// </summary>
        /// <param name="opcode">The opcode to search for.</param>
        /// <returns>A list of instructions with the specified opcode.</returns>
        [DebuggerStepThrough]
        public List<Instruction> FindInstructions(OpcodeType opcode)
        {
            var instructions = new List<Instruction>();

            foreach (var instruction in Instructions)
                if (instruction.Opcode == opcode)
                    instructions.Add(instruction);

            return instructions;
        }

        /// <summary>
        ///     Search for instructions that match a specific set of opcodes.
        /// </summary>
        /// <param name="opcodes">The list of opcodes to search for.</param>
        /// <returns>A list of instructions with the specified opcodes.</returns>
        [DebuggerStepThrough]
        public List<Instruction> FindInstructions(IList<OpcodeType> opcodes)
        {
            var instructions = new List<Instruction>();

            foreach (var instruction in Instructions)
                if (opcodes.Contains(instruction.Opcode))
                    instructions.Add(instruction);

            return instructions;
        }

        /// <summary>
        ///     Search for labels located at the specified offset.
        /// </summary>
        /// <param name="offset">The offset to search for.</param>
        /// <returns>A list of labels located at <paramref name="offset" /></returns>
        [DebuggerStepThrough]
        public List<LabelConstant> FindLabels(uint offset)
        {
            return Block.Labels.Where(l => l.Offset == offset).ToList();
        }

        /// <summary>
        ///     Get the label with the specified index.
        /// </summary>
        /// <param name="labelIndex">The label index to get.</param>
        /// <returns>The label with the specified <paramref name="labelIndex" /></returns>
        [DebuggerStepThrough]
        public LabelConstant GetLabel(int labelIndex)
        {
            return Block.Labels[labelIndex];
        }

        /// <summary>
        ///     Get a label from the the destination of a jump instruction.
        /// </summary>
        /// <param name="instruction">Jump instruction with label.</param>
        /// <returns>The destination label of the specified <paramref name="instruction" /></returns>
        [DebuggerStepThrough]
        public LabelConstant GetDestinationLabel(Instruction instruction)
        {
            switch (instruction.Opcode)
            {
                case OpcodeType.JZ:
                case OpcodeType.JNZ:
                case OpcodeType.JMP:
                case OpcodeType.GOTO:
                    break; // Break if instruction is jump with label
                default:
                    // Return null if instruction is not jump
                    return null;
            }

            return Block.Labels[instruction.Operand];
        }

        /// <summary>
        ///     Get the position of a label in the instruction list.
        /// </summary>
        /// <param name="label">The label to locate.</param>
        /// <returns>The index of the instruction located at the label.</returns>
        [DebuggerStepThrough]
        public int GetLabelPosition(LabelConstant label)
        {
            return Instructions.FindIndex(i => i.Offset == label.Offset);
        }

        /// <summary>
        ///     Get a float constant by index.
        /// </summary>
        /// <param name="floatIndex">Index of the float to retrieve.</param>
        /// <returns>The float constant with the specified index.</returns>
        [DebuggerStepThrough]
        public FloatConstant GetFloat(int floatIndex)
        {
            return Block.Floats[floatIndex];
        }

        /// <summary>
        ///     Get a float constant from an instruction operand.
        /// </summary>
        /// <param name="instruction">Instruction with constant index as operand.</param>
        /// <returns>The float constant with the specified index.</returns>
        [DebuggerStepThrough]
        public FloatConstant GetFloat(Instruction instruction)
        {
            return Block.Floats[instruction.Operand];
        }

        /// <summary>
        ///     Get a float constant from an instruction operand.
        /// </summary>
        /// <param name="offset">Offset of the instruction with constant index as operand.</param>
        /// <returns>The float constant with the specified index.</returns>
        [DebuggerStepThrough]
        public FloatConstant GetFloat(uint offset)
        {
            return Block.Floats[Instructions[offset].Operand];
        }

        /// <summary>
        ///     Get an int constant by index.
        /// </summary>
        /// <param name="intIndex">Index of the int to retrieve.</param>
        /// <returns>The int constant with the specified index.</returns>
        [DebuggerStepThrough]
        public IntConstant GetInt(int intIndex)
        {
            return Block.Integers[intIndex];
        }

        /// <summary>
        ///     Get an int constant from an instruction operand.
        /// </summary>
        /// <param name="instruction">Instruction with constant index as operand.</param>
        /// <returns>The int constant with the specified index.</returns>
        [DebuggerStepThrough]
        public IntConstant GetInt(Instruction instruction)
        {
            return Block.Integers[instruction.Operand];
        }

        /// <summary>
        ///     Get an int constant from an instruction operand.
        /// </summary>
        /// <param name="offset">Offset of the instruction with constant index as operand.</param>
        /// <returns>The int constant with the specified index.</returns>
        [DebuggerStepThrough]
        public IntConstant GetInt(uint offset)
        {
            return Block.Integers[Instructions[offset].Operand];
        }

        /// <summary>
        ///     Get a variable constant by index.
        /// </summary>
        /// <param name="variableIndex">Index of the variable to retrieve.</param>
        /// <returns>The variable constant with the specified index.</returns>
        [DebuggerStepThrough]
        public VariableConstant GetVariable(int variableIndex)
        {
            return Block.Variables[variableIndex];
        }

        /// <summary>
        ///     Get a variable constant from an instruction operand.
        /// </summary>
        /// <param name="instruction">Instruction with constant index as operand.</param>
        /// <returns>The variable constant with the specified index.</returns>
        [DebuggerStepThrough]
        public VariableConstant GetVariable(Instruction instruction)
        {
            return Block.Variables[instruction.Operand];
        }

        /// <summary>
        ///     Get a variable constant from an instruction operand.
        /// </summary>
        /// <param name="offset">Offset of the instruction with constant index as operand.</param>
        /// <returns>The variable constant with the specified index.</returns>
        [DebuggerStepThrough]
        public VariableConstant GetVariable(uint offset)
        {
            return Block.Variables[Instructions[offset].Operand];
        }
    }
}