﻿using System.Collections.Generic;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public abstract class TokenAnalyzer
    {
        public List<int> AnalyzedBytes { get; } = new List<int>();
        public List<LabelConstant> IgnoredLabels { get; } = new List<LabelConstant>();
        
        public bool IsAnalyzed(int position)
        {
            return AnalyzedBytes.Contains(position);
        }

        public bool IsIgnored(LabelConstant label)
        {
            return IgnoredLabels.Contains(label);

        }

        /// <summary>
        /// Commit the analysis state from an existing <paramref name="analyzer"/> to the current analyzer.
        /// </summary>
        /// <param name="analyzer"></param>
        protected void Commit(TokenAnalyzer analyzer)
        {
            foreach(var position in analyzer.AnalyzedBytes)
                if (!AnalyzedBytes.Contains(position))
                    AnalyzedBytes.Add(position);

            foreach(var label in analyzer.IgnoredLabels)
                if (!IgnoredLabels.Contains(label))
                    IgnoredLabels.Add(label);
        }

        protected void MarkAnalyzed(int position)
        {
            if (!AnalyzedBytes.Contains(position))
                AnalyzedBytes.Add(position);
        }

        protected void IgnoreLabel(LabelConstant label)
        {
            if (!IgnoredLabels.Contains(label))
                IgnoredLabels.Add(label);
        }
    }
}