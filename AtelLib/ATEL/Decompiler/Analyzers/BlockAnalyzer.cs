﻿using System.Linq;
using AtelLib.ATEL.Disassembler.Classes;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class BlockAnalyzer
    {
        public AtelDecompiler Decompiler { get; }
        public int BlockId { get; }
        public Block Block { get; }

        public FunctionAnalyzer[] Functions { get; set; }

        public BlockAnalyzer(AtelDecompiler decompiler, int blockId)
        {
            Decompiler = decompiler;
            BlockId = blockId;
            Block = Decompiler.Disassembler.Blocks[blockId];

            LoadFunctions();
        }

        public LabelConstant GetLabel(int labelId)
        {
            return Block.Labels[labelId];
        }

        public LabelConstant FindNextLabel(uint offset)
        {
            return Block.Labels.OrderBy(l => l.Offset).FirstOrDefault(l => l.Offset > offset);
        }

        public LabelConstant FindPreviousLabel(uint offset)
        {
            var ordered = Block.Labels.OrderByDescending(l => l.Offset);
            return ordered.FirstOrDefault(l => l.Offset < offset);
        }

        public void DecompileFunction(int functionId)
        {
            Functions[functionId] = new FunctionAnalyzer(this, functionId);
        }

        private void LoadFunctions()
        {
            Functions = new FunctionAnalyzer[Block.Functions.Count];
            for (var i = 0; i < Block.Functions.Count; i++)
                Functions[i] = new FunctionAnalyzer(this, i);
        }
    }
}