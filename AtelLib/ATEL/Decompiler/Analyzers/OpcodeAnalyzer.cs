﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class OpcodeAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly uint _startOffset;

        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public OpcodeAnalyzer(FunctionAnalyzer fa, uint startOffset)
        {
            _fa = fa;
            _startOffset = startOffset;
            _stack = _fa.Stack;
            _instructions = _fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeOpcode(_startOffset);
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeOpcode(uint offset)
        {
            var instruction = _fa.Instructions[offset];
            var position = _instructions.IndexOf(instruction);
            _fa.Log($"Analyzing {instruction.Opcode} at offset {offset:X4}");
            MarkAnalyzed(position);
            try
            {
                switch (instruction.Opcode)
                {
                    case OpcodeType.OPNOP:
                        return ProcessOPNOP(offset);
                    case OpcodeType.OPLOR:
                        return ProcessOPLOR(offset);
                    case OpcodeType.OPLAND:
                        return ProcessOPLAND(offset);
                    case OpcodeType.OPOR:
                        return ProcessOPOR(offset);
                    case OpcodeType.OPEOR:
                        return ProcessOPEOR(offset);
                    case OpcodeType.OPAND:
                        return ProcessOPAND(offset);
                    case OpcodeType.OPEQ:
                        return ProcessOPEQ(offset);
                    case OpcodeType.OPNE:
                        return ProcessOPNE(offset);
                    case OpcodeType.OPGTU:
                        return ProcessOPGTU(offset);
                    case OpcodeType.OPLSU:
                        return ProcessOPLSU(offset);
                    case OpcodeType.OPGT:
                        return ProcessOPGT(offset);
                    case OpcodeType.OPLS:
                        return ProcessOPLS(offset);
                    case OpcodeType.OPGTEU:
                        return ProcessOPGTEU(offset);
                    case OpcodeType.OPLSEU:
                        return ProcessOPLSEU(offset);
                    case OpcodeType.OPGTE:
                        return ProcessOPGTE(offset);
                    case OpcodeType.OPLSE:
                        return ProcessOPLSE(offset);
                    case OpcodeType.OPBON:
                        return ProcessOPBON(offset);
                    case OpcodeType.OPBOFF:
                        return ProcessOPBOFF(offset);
                    case OpcodeType.OPSLL:
                        return ProcessOPSLL(offset);
                    case OpcodeType.OPSRL:
                        return ProcessOPSRL(offset);
                    case OpcodeType.OPADD:
                        return ProcessOPADD(offset);
                    case OpcodeType.OPSUB:
                        return ProcessOPSUB(offset);
                    case OpcodeType.OPMUL:
                        return ProcessOPMUL(offset);
                    case OpcodeType.OPDIV:
                        return ProcessOPDIV(offset);
                    case OpcodeType.OPMOD:
                        return ProcessOPMOD(offset);
                    case OpcodeType.OPNOT:
                        return ProcessOPNOT(offset);
                    case OpcodeType.OPUMINUS:
                        return ProcessOPUMINUS(offset);
                    case OpcodeType.OPFIXADRS:
                        return ProcessOPFIXADRS(offset);
                    case OpcodeType.OPBNOT:
                        return ProcessOPBNOT(offset);
                    case OpcodeType.LABEL:
                        return ProcessLABEL(offset);
                    case OpcodeType.TAG:
                        return ProcessTAG(offset);
                    case OpcodeType.PUSHV:
                        return ProcessPUSHV(offset);
                    case OpcodeType.POPV:
                        return ProcessPOPV(offset);
                    case OpcodeType.POPVL:
                        return ProcessPOPVL(offset);
                    case OpcodeType.PUSHAR:
                        return ProcessPUSHAR(offset);
                    case OpcodeType.POPAR:
                        return ProcessPOPAR(offset);
                    case OpcodeType.POPARL:
                        return ProcessPOPARL(offset);
                    case OpcodeType.POPA:
                        return ProcessPOPA(offset);
                    case OpcodeType.PUSHA:
                        return ProcessPUSHA(offset);
                    case OpcodeType.PUSHARP:
                        return ProcessPUSHARP(offset);
                    case OpcodeType.PUSHX:
                        return ProcessPUSHX(offset);
                    case OpcodeType.PUSHY:
                        return ProcessPUSHY(offset);
                    case OpcodeType.POPX:
                        return ProcessPOPX(offset);
                    case OpcodeType.REPUSH:
                        return ProcessREPUSH(offset);
                    case OpcodeType.POPY:
                        return ProcessPOPY(offset);
                    case OpcodeType.PUSHI:
                        return ProcessPUSHI(offset);
                    case OpcodeType.PUSHII:
                        return ProcessPUSHII(offset);
                    case OpcodeType.PUSHF:
                        return ProcessPUSHF(offset);
                    case OpcodeType.JMP:
                        return ProcessJMP(offset);
                    case OpcodeType.CJMP:
                        return ProcessCJMP(offset);
                    case OpcodeType.NCJMP:
                        return ProcessNCJMP(offset);
                    case OpcodeType.JSR:
                        return ProcessJSR(offset);
                    case OpcodeType.RTS:
                        return ProcessRTS(offset);
                    case OpcodeType.CALL:
                        return ProcessCALL(offset);
                    case OpcodeType.REQ:
                        return ProcessREQ(offset);
                    case OpcodeType.REQSW:
                        return ProcessREQSW(offset);
                    case OpcodeType.REQEW:
                        return ProcessREQEW(offset);
                    case OpcodeType.PREQ:
                        return ProcessPREQ(offset);
                    case OpcodeType.PREQSW:
                        return ProcessPREQSW(offset);
                    case OpcodeType.PREQEW:
                        return ProcessPREQEW(offset);
                    case OpcodeType.RET:
                        return ProcessRET(offset);
                    case OpcodeType.RETN:
                        return ProcessRETN(offset);
                    case OpcodeType.RETT:
                        return ProcessRETT(offset);
                    case OpcodeType.RETTN:
                        return ProcessRETTN(offset);
                    case OpcodeType.HALT:
                        return ProcessHALT(offset);
                    case OpcodeType.PUSHN:
                        return ProcessPUSHN(offset);
                    case OpcodeType.PUSHT:
                        return ProcessPUSHT(offset);
                    case OpcodeType.PUSHVP:
                        return ProcessPUSHVP(offset);
                    case OpcodeType.PUSHFIX:
                        return ProcessPUSHFIX(offset);
                    case OpcodeType.FREQ:
                        return ProcessFREQ(offset);
                    case OpcodeType.TREQ:
                        return ProcessTREQ(offset);
                    case OpcodeType.BREQ:
                        return ProcessBREQ(offset);
                    case OpcodeType.BFREQ:
                        return ProcessBFREQ(offset);
                    case OpcodeType.BTREQ:
                        return ProcessBTREQ(offset);
                    case OpcodeType.FREQSW:
                        return ProcessFREQSW(offset);
                    case OpcodeType.TREQSW:
                        return ProcessTREQSW(offset);
                    case OpcodeType.BREQSW:
                        return ProcessBREQSW(offset);
                    case OpcodeType.BFREQSW:
                        return ProcessBFREQSW(offset);
                    case OpcodeType.BTREQSW:
                        return ProcessBTREQSW(offset);
                    case OpcodeType.FREQEW:
                        return ProcessFREQEW(offset);
                    case OpcodeType.TREQEW:
                        return ProcessTREQEW(offset);
                    case OpcodeType.BREQEW:
                        return ProcessBREQEW(offset);
                    case OpcodeType.BFREQEW:
                        return ProcessBFREQEW(offset);
                    case OpcodeType.BTREQEW:
                        return ProcessBTREQEW(offset);
                    case OpcodeType.DRET:
                        return ProcessDRET(offset);
                    case OpcodeType.POPXJMP:
                        return ProcessPOPXJMP(offset);
                    case OpcodeType.POPXCJMP:
                        return ProcessPOPXCJMP(offset);
                    case OpcodeType.POPXNCJMP:
                        return ProcessPOPXNCJMP(offset);
                    case OpcodeType.CALLPOPA:
                        return ProcessCALLPOPA(offset);
                    case OpcodeType.POPI0:
                        return ProcessPOPI0(offset);
                    case OpcodeType.POPI1:
                        return ProcessPOPI1(offset);
                    case OpcodeType.POPI2:
                        return ProcessPOPI2(offset);
                    case OpcodeType.POPI3:
                        return ProcessPOPI3(offset);
                    case OpcodeType.POPF0:
                        return ProcessPOPF0(offset);
                    case OpcodeType.POPF1:
                        return ProcessPOPF1(offset);
                    case OpcodeType.POPF2:
                        return ProcessPOPF2(offset);
                    case OpcodeType.POPF3:
                        return ProcessPOPF3(offset);
                    case OpcodeType.POPF4:
                        return ProcessPOPF4(offset);
                    case OpcodeType.POPF5:
                        return ProcessPOPF5(offset);
                    case OpcodeType.POPF6:
                        return ProcessPOPF6(offset);
                    case OpcodeType.POPF7:
                        return ProcessPOPF7(offset);
                    case OpcodeType.POPF8:
                        return ProcessPOPF8(offset);
                    case OpcodeType.POPF9:
                        return ProcessPOPF9(offset);
                    case OpcodeType.PUSHI0:
                        return ProcessPUSHI0(offset);
                    case OpcodeType.PUSHI1:
                        return ProcessPUSHI1(offset);
                    case OpcodeType.PUSHI2:
                        return ProcessPUSHI2(offset);
                    case OpcodeType.PUSHI3:
                        return ProcessPUSHI3(offset);
                    case OpcodeType.PUSHF0:
                        return ProcessPUSHF0(offset);
                    case OpcodeType.PUSHF1:
                        return ProcessPUSHF1(offset);
                    case OpcodeType.PUSHF2:
                        return ProcessPUSHF2(offset);
                    case OpcodeType.PUSHF3:
                        return ProcessPUSHF3(offset);
                    case OpcodeType.PUSHF4:
                        return ProcessPUSHF4(offset);
                    case OpcodeType.PUSHF5:
                        return ProcessPUSHF5(offset);
                    case OpcodeType.PUSHF6:
                        return ProcessPUSHF6(offset);
                    case OpcodeType.PUSHF7:
                        return ProcessPUSHF7(offset);
                    case OpcodeType.PUSHF8:
                        return ProcessPUSHF8(offset);
                    case OpcodeType.PUSHF9:
                        return ProcessPUSHF9(offset);
                    case OpcodeType.PUSHAINTER:
                        return ProcessPUSHAINTER(offset);
                    case OpcodeType.SYSTEM:
                        return ProcessSYSTEM(offset);
                    case OpcodeType.REQWAIT:
                        return ProcessREQWAIT(offset);
                    case OpcodeType.PREQWAIT:
                        return ProcessPREQWAIT(offset);
                    case OpcodeType.REQCHG:
                        return ProcessREQCHG(offset);
                    case OpcodeType.ACTREQ:
                        return ProcessACTREQ(offset);
                    case OpcodeType.VPUSH:
                        return ProcessVPUSH(offset);
                    case OpcodeType.VPOP:
                        return ProcessVPOP(offset);
                    case OpcodeType.OPA1:
                        return ProcessOPA1(offset);
                    case OpcodeType.ARRPUSH:
                        return ProcessARRPUSH(offset);
                    case OpcodeType.ARRPOP:
                        return ProcessARRPOP(offset);
                    case OpcodeType.OPA7:
                        return null; // Unknown opcode
                    case OpcodeType.IPUSH:
                        return ProcessIPUSH(offset);
                    case OpcodeType.PUSH:
                        return ProcessPUSH(offset);
                    case OpcodeType.FPUSH:
                        return ProcessFPUSH(offset);
                    case OpcodeType.GOTO:
                        return ProcessGOTO(offset);
                    case OpcodeType.OPB3:
                        return null; // Unknown opcode
                    case OpcodeType.EXEC:
                        return ProcessEXEC(offset);
                    case OpcodeType.JNZ:
                        return ProcessJNZ(offset);
                    case OpcodeType.JZ:
                        return ProcessJZ(offset);
                    case OpcodeType.SUB:
                        return ProcessSUB(offset);
                    default:
                        throw new Exception($"Undefined opcode: {instruction.Opcode:X2}");
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.Contains("Stack empty"))
                    throw new Exception($"Analysis failed, stack empty.");
                throw;
            }
        }

        private IToken ProcessOPNOP(uint offset)
        {
            var instruction = _instructions[offset];
            // No operation
            return null;
        }

        private IToken ProcessOPLOR(uint offset)
        {
            var instruction = _instructions[offset];
            // Logical OR
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LOR, operand1, operand2));
            return null;
        }

        private IToken ProcessOPLAND(uint offset)
        {
            var instruction = _instructions[offset];
            // Logical AND
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LAND, operand1, operand2));
            return null;
        }

        private IToken ProcessOPOR(uint offset)
        {
            var instruction = _instructions[offset];
            // Bitwise OR
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.OR, operand1, operand2));
            return null;
        }

        private IToken ProcessOPEOR(uint offset)
        {
            var instruction = _instructions[offset];
            // Bitwise XOR
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.EOR, operand1, operand2));
            return null;
        }

        private IToken ProcessOPAND(uint offset)
        {
            var instruction = _instructions[offset];
            // Bitwise AND
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.AND, operand1, operand2));
            return null;
        }

        private IToken ProcessOPEQ(uint offset)
        {
            var instruction = _instructions[offset];
            // Equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.EQ, operand1, operand2));
            return null;
        }

        private IToken ProcessOPNE(uint offset)
        {
            var instruction = _instructions[offset];
            // Not equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.NE, operand1, operand2));
            return null;
        }

        private IToken ProcessOPGTU(uint offset)
        {
            var instruction = _instructions[offset];
            // Unsigned greater than
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.GTU, operand1, operand2));
            return null;
        }

        private IToken ProcessOPLSU(uint offset)
        {
            var instruction = _instructions[offset];
            // Unsigned less than
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LSU, operand1, operand2));
            return null;
        }

        private IToken ProcessOPGT(uint offset)
        {
            var instruction = _instructions[offset];
            // Greater than
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.GT, operand1, operand2));
            return null;
        }

        private IToken ProcessOPLS(uint offset)
        {
            var instruction = _instructions[offset];
            // Less than
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LS, operand1, operand2));
            return null;
        }

        private IToken ProcessOPGTEU(uint offset)
        {
            var instruction = _instructions[offset];
            // Unsigned greater than or equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.GTEU, operand1, operand2));
            return null;
        }

        private IToken ProcessOPLSEU(uint offset)
        {
            var instruction = _instructions[offset];
            // Unsigned less than or equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LSEU, operand1, operand2));
            return null;
        }

        private IToken ProcessOPGTE(uint offset)
        {
            var instruction = _instructions[offset];
            // Greater than or equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.GTE, operand1, operand2));
            return null;
        }

        private IToken ProcessOPLSE(uint offset)
        {
            var instruction = _instructions[offset];
            // Less than or equal
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.LSE, operand1, operand2));
            return null;
        }

        private IToken ProcessOPBON(uint offset)
        {
            var instruction = _instructions[offset];
            // Bit on
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.BON, operand1, operand2));
            return null;
        }

        private IToken ProcessOPBOFF(uint offset)
        {
            var instruction = _instructions[offset];
            // Bit off
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.BOFF, operand1, operand2));
            return null;
        }

        private IToken ProcessOPSLL(uint offset)
        {
            var instruction = _instructions[offset];
            // Shift left
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.SLL, operand1, operand2));
            return null;
        }

        private IToken ProcessOPSRL(uint offset)
        {
            var instruction = _instructions[offset];
            // Shift right
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.SRL, operand1, operand2));
            return null;
        }

        private IToken ProcessOPADD(uint offset)
        {
            var instruction = _instructions[offset];
            // Add
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.ADD, operand1, operand2));
            return null;
        }

        private IToken ProcessOPSUB(uint offset)
        {
            var instruction = _instructions[offset];
            // Subtract
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.SUB, operand1, operand2));
            return null;
        }

        private IToken ProcessOPMUL(uint offset)
        {
            var instruction = _instructions[offset];
            // Multiply
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.MUL, operand1, operand2));
            return null;
        }

        private IToken ProcessOPDIV(uint offset)
        {
            var instruction = _instructions[offset];
            // Divide
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.DIV, operand1, operand2));
            return null;
        }

        private IToken ProcessOPMOD(uint offset)
        {
            var instruction = _instructions[offset];
            // Modulo
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.MOD, operand1, operand2));
            return null;
        }

        private IToken ProcessOPNOT(uint offset)
        {
            var instruction = _instructions[offset];
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.NOT, operand1));
            return null;
        }

        private IToken ProcessOPUMINUS(uint offset)
        {
            var instruction = _instructions[offset];
            // Unsigned minus (???)
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.UMINUS, operand1, operand2));
            return null;
        }

        private IToken ProcessOPFIXADRS(uint offset)
        {
            var instruction = _instructions[offset];
            // (???)
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.FIXADRS, operand1, operand2));
            return null;
        }

        private IToken ProcessOPBNOT(uint offset)
        {
            var instruction = _instructions[offset];
            // Bitwise NOT (???)
            var operand2 = _stack.Pop();
            var operand1 = _stack.Pop();
            _stack.Push(new Operation(instruction.Offset, OperatorType.BNOT, operand1, operand2));
            return null;
        }

        private IToken ProcessLABEL(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessTAG(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPUSHV(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPV(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPVL(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPUSHAR(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHAR"));
            return null;
        }

        private IToken ProcessPOPAR(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPARL(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPA(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPUSHA(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHA"));
            return null;
        }

        private IToken ProcessPUSHARP(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHARP"));
            return null;
        }

        private IToken ProcessPUSHX(uint offset)
        {
            return null;
        }

        private IToken ProcessPUSHY(uint offset)
        {
            return null;
        }

        private IToken ProcessPOPX(uint offset)
        {
            // I thought this started a switch but it appears not
            MarkAnalyzed(_instructions.FindPosition(offset));
            return null;
            // Start switch statement
            var condition = _stack.Pop();
            var @goto = _instructions[offset + 1];
            MarkAnalyzed(_instructions.FindPosition(offset + 1));
            if (@goto.Opcode != OpcodeType.GOTO)
                throw new Exception("Encountered POPY without subsequent GOTO.");

            var label = _fa.GetDestinationLabel(@goto);
            _fa.Log($"Found switch conditional at {offset:X4}, switch block is located at {label.Offset:X4}.");
            var analyzer = new SwitchAnalyzer(_fa, label, condition);
            analyzer.Analyze();
            Commit(analyzer);
            return analyzer.Result;
        }

        private IToken ProcessREPUSH(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(_stack.Peek());
            return null;
        }

        private IToken ProcessPOPY(uint offset)
        {
            // Start switch statement
            var condition = _stack.Pop();
            var @goto = _instructions[offset + 1];
            MarkAnalyzed(_instructions.FindPosition(offset + 1));
            if (@goto.Opcode != OpcodeType.GOTO)
                throw new Exception("Encountered POPY without subsequent GOTO.");

            var label = _fa.GetDestinationLabel(@goto);
            _fa.Log($"Found switch conditional at {offset:X4}, switch block is located at {label.Offset:X4}.");
            var analyzer = new SwitchAnalyzer(_fa, label, condition);
            analyzer.Analyze();
            Commit(analyzer);
            return analyzer.Result;
        }

        private IToken ProcessPUSHI(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHI"));
            return null;
        }

        private IToken ProcessPUSHII(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHII"));
            return null;
        }

        private IToken ProcessPUSHF(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHF"));
            return null;
        }

        private IToken ProcessJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessCJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessNCJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessJSR(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessRTS(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessCALL(uint offset)
        {
            var analyzer = new CallAnalyzer(_fa, offset);
            analyzer.Analyze();
            Commit(analyzer);
            if(analyzer.HasResult)
                _stack.Push(analyzer.Result);
            return null;
        }

        private IToken ProcessREQ(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request remote execution (asynchronous, non-guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"REQ({id}, {priority}, {function})");
        }

        private IToken ProcessREQSW(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request remote execution (asynchronous execution, guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"REQSW({id}, {priority}, {function})");
        }

        private IToken ProcessREQEW(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request remote execution (synchronous, guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"REQEW({id}, {priority}, {function})");
        }

        private IToken ProcessPREQ(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request party entity execution (asynchronous, non-guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"PREQ({id}, {priority}, {function})");
        }

        private IToken ProcessPREQSW(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request party entity execution (asynchronous execution, guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"PREQSW({id}, {priority}, {function})");
        }

        private IToken ProcessPREQEW(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Request party entity execution(synchronous, guaranteed)
            var instruction = _instructions[offset];
            var function = _stack.Pop();
            var priority = _stack.Pop();
            var id = _stack.Pop();

            return new GenericToken(instruction.Offset, $"PREQEW({id}, {priority}, {function})");
        }

        private IToken ProcessRET(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            return new Return(instruction.Offset);
        }

        private IToken ProcessRETN(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            return new Return(instruction.Offset);
        }

        private IToken ProcessRETT(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            // Return to??
            var instruction = _instructions[offset];
            return new Return(instruction.Offset);
        }

        private IToken ProcessRETTN(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            return new Return(instruction.Offset);
        }

        private IToken ProcessHALT(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPUSHN(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHN"));
            return null;
        }

        private IToken ProcessPUSHT(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHT"));
            return null;
        }

        private IToken ProcessPUSHVP(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHVP"));
            return null;
        }

        private IToken ProcessPUSHFIX(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHFIX"));
            return null;
        }

        private IToken ProcessFREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessTREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBFREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBTREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessFREQSW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessTREQSW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBREQSW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBFREQSW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBTREQSW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessFREQEW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessTREQEW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBREQEW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBFREQEW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessBTREQEW(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessDRET(uint offset)
        {
            var instruction = _instructions[offset];
            return new Return(instruction.Offset);
        }

        private IToken ProcessPOPXJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPXCJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPXNCJMP(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessCALLPOPA(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPOPI0(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);
            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.I0, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPI1(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.I1, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPI2(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.I2, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPI3(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.I3, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF0(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF1(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF2(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF3(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF4(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF5(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF6(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF7(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF8(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPOPF9(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var instruction = _instructions[offset];
            var pop = new SetRegister(instruction.Offset, RegisterType.F9, _stack.Pop());
            return pop;
        }

        private IToken ProcessPUSHI0(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.I0));
            return null;
        }

        private IToken ProcessPUSHI1(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.I1));
            return null;
        }

        private IToken ProcessPUSHI2(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.I2));
            return null;
        }

        private IToken ProcessPUSHI3(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.I3));
            return null;
        }

        private IToken ProcessPUSHF0(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F0));
            return null;
        }

        private IToken ProcessPUSHF1(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F1));
            return null;
        }

        private IToken ProcessPUSHF2(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F2));
            return null;
        }

        private IToken ProcessPUSHF3(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F3));
            return null;
        }

        private IToken ProcessPUSHF4(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F4));
            return null;
        }

        private IToken ProcessPUSHF5(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F5));
            return null;
        }

        private IToken ProcessPUSHF6(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F6));
            return null;
        }

        private IToken ProcessPUSHF7(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F7));
            return null;
        }

        private IToken ProcessPUSHF8(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F8));
            return null;
        }

        private IToken ProcessPUSHF9(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GetRegister(instruction.Offset, RegisterType.F9));
            return null;
        }

        private IToken ProcessPUSHAINTER(uint offset)
        {
            var instruction = _instructions[offset];
            _stack.Push(new GenericToken(instruction.Offset, "PUSHAINTER"));
            return null;
        }

        private IToken ProcessSYSTEM(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessREQWAIT(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessPREQWAIT(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessREQCHG(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessACTREQ(uint offset)
        {
            var instruction = _instructions[offset];
            return null;
        }

        private IToken ProcessVPUSH(uint offset)
        {
            // Push value from variable onto the stack
            _stack.Push(new GetVariable(offset, _fa.GetVariable(offset)));
            return null;
        }

        private IToken ProcessVPOP(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var variable = _fa.GetVariable(offset);
            var vpop = new SetVariable(offset, variable, _stack.Pop());
            return vpop;
        }

        private IToken ProcessOPA1(uint offset)
        {
            // Unknown opcode, appears in field script
            return null;
        }

        private IToken ProcessARRPUSH(uint offset)
        {
            var variable = _fa.GetVariable(offset);
            var index = _stack.Pop();
            _stack.Push(new GetVariable(offset, variable, index));
            return null;
        }

        private IToken ProcessARRPOP(uint offset)
        {
            var position = _instructions.FindPosition(offset);
            MarkAnalyzed(position);

            var variable = _fa.GetVariable(offset);
            var value = _stack.Pop();
            var index = _stack.Pop();
            var arrpop = new SetVariable(offset, variable, value, index);
            return arrpop;
        }

        private IToken ProcessIPUSH(uint offset)
        {
            // Push constant int value onto the stack
            var @int = _fa.GetInt(offset);
            _stack.Push(new GetConstantInt(offset, @int));
            return null;
        }

        private IToken ProcessPUSH(uint offset)
        {
            // Push value from operand onto the stack
            _stack.Push(new Value(offset, _instructions[offset].Operand));
            return null;
        }

        private IToken ProcessFPUSH(uint offset)
        {
            // Push constant float value onto the stack
            var @float = _fa.GetFloat(offset);
            _stack.Push(new GetConstantFloat(offset, @float));
            return null;
        }

        private IToken ProcessGOTO(uint offset)
        {
            var instruction = _instructions[offset];
            var position = _instructions.IndexOf(instruction);
            MarkAnalyzed(position);
            var @goto = new Goto(offset, _fa.GetDestinationLabel(instruction));
            return @goto;
        }

        private IToken ProcessEXEC(uint offset)
        {
            // Call with return value
            var analyzer = new CallAnalyzer(_fa, offset);
            analyzer.Analyze();
            Commit(analyzer);
            if(analyzer.HasResult)
                _stack.Push(analyzer.Result);
            return null;
        }

        private IToken ProcessJNZ(uint offset)
        {
            // Jump if not zero
            var analyzer = new JumpAnalyzer(_fa, offset);
            analyzer.Analyze();
            Commit(analyzer);
            return analyzer.Result;
        }

        private IToken ProcessJZ(uint offset)
        {
            // Jump if zero
            var analyzer = new JumpAnalyzer(_fa, offset);
            analyzer.Analyze();
            Commit(analyzer);
            return analyzer.Result;
        }

        private IToken ProcessSUB(uint offset)
        {
            // Call without return value
            var analyzer = new CallAnalyzer(_fa, offset);
            analyzer.Analyze();
            Commit(analyzer);
            return analyzer.Result;
        }
    }
}