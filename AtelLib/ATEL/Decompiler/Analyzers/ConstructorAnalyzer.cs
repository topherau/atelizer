﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class ConstructorAnalyzer
    {
        private readonly FunctionAnalyzer _fa;

        public ConstructorAnalyzer(FunctionAnalyzer fa)
        {
            _fa = fa;
        }

        public List<IToken> AnalyzeConstructor()
        {
            var block = _fa.Block;
            var variables = block.Variables;

            var constructor = new List<IToken>();

            //// declare all variables for this block
            //foreach (var variable in variables.Where(v => (v.Flags & VariableFlags.Block) != 0 && (v.Flags & VariableFlags.Global) == 0))
            //    constructor.Add(new DeclareVariable(variable));
            
            return constructor;
        }
    }
}