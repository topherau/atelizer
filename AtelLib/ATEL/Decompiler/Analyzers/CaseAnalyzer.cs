﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class CaseAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly LabelConstant _startLabel;
        private readonly int _length;
        private readonly LabelConstant _breakLabel;
        private readonly IToken _condition;
        
        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public CaseAnalyzer(FunctionAnalyzer fa, LabelConstant startLabel, int length, LabelConstant breakLabel, IToken condition)
        {
            _fa = fa;
            _startLabel = startLabel;
            _length = length;
            _breakLabel = breakLabel;
            _condition = condition;
            _instructions = fa.Instructions;
            _stack = fa.Stack;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeCase();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeCase()
        {
            _fa.Log($"Analyzing case with condition \"{_condition}\" at offset {_startLabel.Offset}");
            var startPosition = _fa.GetLabelPosition(_startLabel);
            var currentPosition = startPosition;
            var children = new List<IToken>();
            for (; currentPosition < startPosition + _length; currentPosition++)
            {
                if (IsAnalyzed(currentPosition))
                    continue;

                MarkAnalyzed(currentPosition);

                var currentInstruction = _instructions[currentPosition];

                // Add all labels inside the case
                var labels = _fa.FindLabels(currentInstruction.Offset)
                    .Select(l => new Label(l.Offset, l));
                children.AddRange(labels);

                var analyzer = new OpcodeAnalyzer(_fa, currentInstruction.Offset);
                analyzer.Analyze();
                Commit(analyzer);

                // Continue if no result produces
                if (!analyzer.HasResult)
                    continue;

                // Reached goto at end of case
                if (analyzer.Result is Goto gotoToken)
                {
                    // Check if goto destination is break label (end of switch block)
                    if(gotoToken.Destination.Offset == _breakLabel.Offset)
                    {
                        // Add break token
                        children.Add(new Break(gotoToken.Offset));
                        break;
                    } else
                    {
                        // Add goto token
                        children.Add(gotoToken);
                        break;
                    }
                }
                
                children.Add(analyzer.Result);

                // End case if return was found
                if (analyzer.Result is Return)
                    break;
            }

            return new Case(_startLabel.Offset, _startLabel, _condition, children);
        }
    }
}