﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;
using AtelLib.ATEL.Disassembler.Classes.Constants;
using AtelLib.ATEL.Disassembler.Enums;
using Switch = AtelLib.ATEL.Decompiler.Tokens.Conditionals.Switch;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class SwitchAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly LabelConstant _blockStart;
        private readonly IToken _condition;
        
        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;

        [DebuggerStepThrough]
        public SwitchAnalyzer(FunctionAnalyzer fa, LabelConstant blockStart, IToken condition)
        {
            _fa = fa;
            _blockStart = blockStart;
            _condition = condition;
            _stack = fa.Stack;
            _instructions = fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeSwitch();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeSwitch()
        {
            _fa.Log($"Analyzing switch block at offset {_blockStart.Offset:X4}");
            var startInstruction = _instructions[_blockStart.Offset];
            var startPosition = _instructions.IndexOf(startInstruction);

            // Forward search for next label, as that will be the end of the switch block
            var switchBlockEnd = _fa.BlockAnalyzer.FindNextLabel(_blockStart.Offset);
            
            // Initial boundary is end of instruction list
            var switchBlockEndPos = _instructions.Count;
            if (switchBlockEnd != null) // If we found a label, set the boundary there
            {
                switchBlockEndPos = _instructions.FindIndex(i => i.Offset == switchBlockEnd.Offset);
                IgnoreLabel(switchBlockEnd);
            }

            var cases = new List<Case>(); //        List of cases in switch
            var caseStack = new Stack<IToken>(); // Temporary stack to build case conditions
            var switchInstructions = new List<Instruction>();
            // Find the position of the switch block's initial instruction
            var currentPosition = startPosition;

            // Iterate instructions in switch block to build initial list of cases
            for (; currentPosition < switchBlockEndPos; currentPosition++)
            {
                MarkAnalyzed(currentPosition);

                var instruction = _instructions[currentPosition];
                switchInstructions.Add(instruction);
                switch (instruction.Opcode)
                {
                    case OpcodeType.IPUSH:
                    {
                        caseStack.Push(new GetConstantInt(instruction.Offset, _fa.GetInt(instruction)));
                        continue;
                    }
                    case OpcodeType.PUSH: //    Push case value
                    {
                        caseStack.Push(new Value(instruction.Offset, instruction.Operand));
                        continue;
                    }
                    case OpcodeType.PUSHY: //   Push control value
                    {
                        caseStack.Push(new GetRegister(instruction.Offset, RegisterType.Y));
                        continue;
                    }
                    case OpcodeType.PUSHX: //   Push control value
                    {
                        caseStack.Push(new GetRegister(instruction.Offset, RegisterType.X));
                        continue;
                    }
                    case OpcodeType.OPEQ: //    Comparison for case jump
                    {
                        caseStack.Push(new Operation(instruction.Offset,
                            OperatorType.EQ, caseStack.Pop(), caseStack.Pop()));
                        continue;
                    }
                    case OpcodeType.OPGT: //    Comparison for case jump
                    {
                        caseStack.Push(new Operation(instruction.Offset,
                            OperatorType.GT, caseStack.Pop(), caseStack.Pop()));
                        continue;
                    }
                    case OpcodeType.OPGTE: //   Comparison for case jump
                    {
                        caseStack.Push(new Operation(instruction.Offset,
                            OperatorType.GTE, caseStack.Pop(), caseStack.Pop()));
                        continue;
                    }
                    case OpcodeType.OPLS: //    Comparison for case jump
                    {
                        caseStack.Push(new Operation(instruction.Offset,
                            OperatorType.LS, caseStack.Pop(), caseStack.Pop()));
                        continue;
                    }
                    case OpcodeType.OPLSE: //   Comparison for case jump
                    {
                        caseStack.Push(new Operation(instruction.Offset,
                            OperatorType.LSE, caseStack.Pop(), caseStack.Pop()));
                        continue;
                    }
                    case OpcodeType.JZ:
                    case OpcodeType.JNZ: //     Jump to case label
                    {
                        var condition = caseStack.Pop();
                        if (instruction.Opcode == OpcodeType.JZ)
                            condition = new Operation(instruction.Offset, OperatorType.NOT, condition);
                        var label = _fa.GetDestinationLabel(instruction);
                        IgnoreLabel(label);
                        // Check if case is fallthrough, i.e. shares offset with a previous case
                        var fallThrough = cases.LastOrDefault(c => c.Offset == label.Offset);
                        if (fallThrough != null)
                        {
                            // Ensure that fallthrough came from last case
                            var index = cases.IndexOf(fallThrough);
                            if (index != cases.Count - 1)
                                throw new Exception(
                                    $"Invalid fallthrough from case \"{fallThrough.Condition}\" into case \"{condition}\".");

                            _fa.Log(
                                $"Found fallthrough from case \"{fallThrough.Condition}\" into case \"{condition}\".");
                        }
                        else
                        {
                            _fa.Log($"Found case \"{condition}\" at offset {label.Offset:X4}");
                        }

                        cases.Add(new Case(label.Offset, label, condition, null));
                        continue;
                    }
                    case OpcodeType.GOTO: //    Jump to default case
                    {
                        var @default = _fa.GetDestinationLabel(instruction);
                        IgnoreLabel(@default);
                            cases.Add(new Case(@default.Offset, @default, null, null));
                        break; //               After default case, fall through to end switch check
                    }
                    case OpcodeType.RET:
                        goto endSwitchBlock;
                    default: //                 Unknown instruction
                        throw new Exception($"Unknown instruction in switch block: {instruction.Opcode}.");
                }

                // Check that default case was really the last instruction in thecase block
                if (currentPosition != switchBlockEndPos - 1)
                    throw new Exception("Reached default case but it wasn't the last instruction in the switch block!");
            }

            endSwitchBlock:

            var jumpOverSwitch = _instructions[startPosition - 1];
            if (jumpOverSwitch.Opcode == OpcodeType.GOTO)
            {
                var jumpOverLabel = _fa.GetDestinationLabel(jumpOverSwitch);
                var jumpOverDestination = _instructions.FindPosition(jumpOverLabel.Offset);
                if (jumpOverDestination == switchBlockEndPos)
                {
                    MarkAnalyzed(startPosition - 1);
                    IgnoreLabel(jumpOverLabel);
                }
            }

            // Group cases by offset
            var casesByOffset = cases.GroupBy(c => c.Offset).ToList();
            _fa.Log($"Analyzing {cases.Count} cases ({casesByOffset.Count()} code blocks)...");

            // Analyze case statements
            var finalCases = new List<Case>();
            for (var i = 0; i < casesByOffset.Count; i++)
            {
                var caseCount = casesByOffset[i].Count();
                Case lastCase = null;
                for (var k = 0; k < caseCount; k++)
                {
                    // Check if's the last case at this offset
                    if (k == caseCount - 1)
                    {
                        // Last case will contain analyzed block,
                        lastCase = casesByOffset[i].ElementAt(k);
                    }
                    else
                    {
                        // Fallthrough cases have null children, just add dummy case from above
                        var cc = casesByOffset[i].ElementAt(k);
                        finalCases.Add(cc);
                    }
                }

                // If last case is null, there were probably no children in the case
                // This should never happen, but let's avoid a crash here
                if (lastCase == null)
                    continue;

                // Analyze case instructions
                int caseLength;
                var caseStartPos = _instructions.FindPosition(lastCase.Offset);

                // Determine length of case block
                if (i == casesByOffset.Count - 1)
                {
                    // Last case in switch, block is from case block start to switch block start
                    caseLength = startPosition - caseStartPos;
                }
                else
                {
                    // Any case other than the last
                    var endOffset = casesByOffset[i + 1].Key;
                    var nextSwitchStart = _instructions.FindIndex(n => n.Offset == endOffset);
                    caseLength = nextSwitchStart - caseStartPos;
                }

                var analyzer = new CaseAnalyzer(_fa, lastCase.StartLabel, caseLength, switchBlockEnd,
                    lastCase.Condition);
                analyzer.Analyze();
                Commit(analyzer);
                finalCases.Add((Case) analyzer.Result);
            }

            var jumpsToSwitch = _fa.FindJumpsTo(_blockStart);
            if (jumpsToSwitch.Count != 1)
                throw new Exception("More than one jump to switch block!");

            return new Switch(jumpsToSwitch[0].Offset, _condition, finalCases, switchInstructions);
        }
    }
}