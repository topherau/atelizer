﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Classes;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;

namespace AtelLib.ATEL.Decompiler.Analyzers
{
    public class JumpAnalyzer : TokenAnalyzer, ITokenAnalyzer
    {
        private readonly FunctionAnalyzer _fa;
        private readonly InstructionList _instructions;
        private readonly Stack<IToken> _stack;
        private readonly uint _startOffset;

        [DebuggerStepThrough]
        public JumpAnalyzer(FunctionAnalyzer fa, uint startOffset)
        {
            _fa = fa;
            _startOffset = startOffset;
            _stack = _fa.Stack;
            _instructions = _fa.Instructions;
        }

        public bool HasResult { get; private set; }
        public IToken Result { get; private set; }

        public bool Analyze()
        {
            Result = AnalyzeJump();
            HasResult = Result != null;
            return HasResult;
        }

        private IToken AnalyzeJump()
        {
            // Get instruction at position (the jump instruction)
            var instruction = _instructions[_startOffset];
            var conditional = _stack.Pop();

            // Calculate end position for branch from destination of jump
            var branchEndLabel = _fa.GetDestinationLabel(instruction);
            var branchEndPos = _fa.GetLabelPosition(branchEndLabel);
            IgnoreLabel(branchEndLabel);

            _fa.Log($"Analyzing jump from {_startOffset:X4} to {branchEndLabel.Offset:X4} with conditional \"{conditional}\"");
            _fa.Log("Analyzing inner branch...");

            // Pop conditional and analyze instructions within block

            var children = new List<IToken>();
            var currentPosition = _instructions.IndexOf(instruction) + 1; // position immediately after jump instruction
            for (; currentPosition < branchEndPos; currentPosition++)
            {
                if (IsAnalyzed(currentPosition))
                    continue;

                MarkAnalyzed(currentPosition);

                // Get instruction at current position
                var currentInstruction = _instructions[currentPosition];

                // Add labels for current offset
                var labels = _fa.FindLabels(currentInstruction.Offset)
                    .Select(l => new Label(l.Offset, l));
                children.AddRange(labels);

                var analyzer = new OpcodeAnalyzer(_fa, currentInstruction.Offset);
                analyzer.Analyze();
                Commit(analyzer);

                if (!analyzer.HasResult)
                    continue;

                // Analysis produced a token
                var token = analyzer.Result;

                // Search for while loop
                if (token is Goto gotoToken && gotoToken.Destination.Offset < currentInstruction.Offset)
                {
                    _fa.Log("Found GOTO at end of branch that points backwards, searching for while loop...");
                    var gotoAnalyzer = new WhileAnalyzer(_fa, currentInstruction.Offset,
                        WhileAnalyzer.AnalyzeMode.WithoutConditional);
                    gotoAnalyzer.Analyze();
                    if (gotoAnalyzer.AdjustedResult != null)
                    {
                        children = gotoAnalyzer.AdjustedResult;
                        continue;
                    }
                }

                // Add token to children
                children.Add(token);
            }


            // No children to check, return standard if statement
            if (children.Count == 0)
            {
                _fa.Log($"Branch at {_startOffset} is \"if({conditional})\" with no children or additional branches.");
                return new If(_startOffset, conditional, children);
            }

            // Perform additional checks to determine additional branches
            _fa.Log($"Reached end of branch, checking for additional branches...");
            var lastChild = children[children.Count - 1];

            // If last child is GOTO, we need determine if it makes an else statement
            if (!(lastChild is Goto finalGoto))
            {
                _fa.Log($"No additional branches found.");
                return new If(_startOffset, conditional, children);
            }

            // First check if the goto points back to the conditional, if so it's a while loop
            if (finalGoto.Destination.Offset == conditional.StartOffset)
            {
                _fa.Log($"Found while loop to {conditional.StartOffset} with conditional \"{conditional}\".");
                return new While(instruction.Offset, conditional, children);
            }

            // Check if the goto points back to any offset before itself (other than the conditional)
            // If so, it's an if with a goto at the end
            if (finalGoto.Destination.Offset < lastChild.Offset)
            {
                _fa.Log($"Found regular goto statement to {finalGoto.Destination.Name} at {finalGoto.Destination.Offset:X4}.");
                return new If(instruction.Offset, conditional, children);
            }

            _fa.Log($"Final child in if statement is GOTO {finalGoto.Destination:X4}, analyzing following instructions to determine else condition");
            var elseAnalyzer = new ElseAnalyzer(_fa, branchEndLabel, finalGoto.Destination);
            elseAnalyzer.Analyze();

            if (elseAnalyzer.HasResult)
            {
                var @else = (Else)elseAnalyzer.Result;
                if (@else.Children.Count != 0)
                {
                    var lastChildIndex = _instructions.FindPosition(@else.Children.Last().EndOffset);
                    var nextInstruction = _instructions[lastChildIndex + 1];
                    if (nextInstruction.Offset == finalGoto.Destination.Offset)
                    {
                        // Branches meet up at same point, it's an else case
                        Commit(elseAnalyzer);
                        children.Remove(finalGoto);
                        IgnoreLabel(finalGoto.Destination);
                        if (children.Count == 0)
                        {
                            // No children in if, but with else case, invert if conditional and use else case as branch
                            return new If(instruction.Offset, new Operation(conditional.Offset, OperatorType.NOT, conditional), @else.Children);
                        }
                        return new If(instruction.Offset, conditional, children, @else);
                    }
                } else if (@else.EndOffset == finalGoto.Destination.Offset)
                {
                    Commit(elseAnalyzer);
                    children.Remove(finalGoto);
                    IgnoreLabel(finalGoto.Destination);
                    if (children.Count == 0)
                    {
                        // No children in if, but with else case, invert if conditional and use else case as branch
                        return new If(instruction.Offset, new Operation(conditional.Offset, OperatorType.NOT, conditional), @else.Children); 
                    }
                    return new If(instruction.Offset, conditional, children, @else);
                }
            }
            
            return new If(instruction.Offset, conditional, children);
        }
    }
}