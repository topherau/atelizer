﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public sealed class Operation : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => LeftOperand?.StartOffset ?? Offset;
        public uint EndOffset => Offset;
        public OperatorType Type { get; set; }
        public IToken LeftOperand { get; set; }
        public IToken RightOperand { get; set; }

        [DebuggerStepThrough]
        public Operation(uint offset, OperatorType type, IToken leftOperand, IToken rightOperand = null)
        {
            Offset = offset;
            Type = type;
            LeftOperand = leftOperand;
            RightOperand = rightOperand;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            if (Type == OperatorType.NOT)
                return $"!({LeftOperand})";
            
            return $"{LeftOperand} {GetOperatorString()} {RightOperand}";
        }

        [DebuggerStepThrough]
        public string GetOperatorString()
        {
            string op;

            switch (Type)
            {
                case OperatorType.LOR:
                    op = "||";
                    break;
                case OperatorType.LAND:
                    op = "&&";
                    break;
                case OperatorType.OR:
                    op = "|";
                    break;
                case OperatorType.EOR:
                    op = "^";
                    break;
                case OperatorType.AND:
                    op = "&";
                    break;
                case OperatorType.EQ:
                    op = "==";
                    break;
                case OperatorType.NE:
                    op = "!=";
                    break;
                case OperatorType.GTU:
                    op = ">";
                    break;
                case OperatorType.LSU:
                    op = "<";
                    break;
                case OperatorType.GT:
                    op = ">";
                    break;
                case OperatorType.LS:
                    op = "<";
                    break;
                case OperatorType.GTEU:
                    op = ">=";
                    break;
                case OperatorType.LSEU:
                    op = "<=";
                    break;
                case OperatorType.GTE:
                    op = ">=";
                    break;
                case OperatorType.LSE:
                    op = "<=";
                    break;
                case OperatorType.BON:
                    op = "BON";
                    break;
                case OperatorType.BOFF:
                    op = "BOFF";
                    break;
                case OperatorType.SLL:
                    op = "<<";
                    break;
                case OperatorType.SRL:
                    op = ">>";
                    break;
                case OperatorType.ADD:
                    op = "+";
                    break;
                case OperatorType.SUB:
                    op = "-";
                    break;
                case OperatorType.MUL:
                    op = "*";
                    break;
                case OperatorType.DIV:
                    op = "/";
                    break;
                case OperatorType.MOD:
                    op = "%";
                    break;
                case OperatorType.NOT:
                    op = "!";
                    break;
                case OperatorType.UMINUS:
                    op = "UMINUS";
                    break;
                case OperatorType.FIXADRS:
                    op = "FIXADRS";
                    break;
                case OperatorType.BNOT:
                    op = "BNOT";
                    break;
                default:
                    op = "???";
                    break;
            }

            return op;
        }
    }
}