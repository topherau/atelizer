﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public sealed class Label : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public LabelConstant SourceLabel { get; set; }
        public bool IsIgnored { get; set; }

        [DebuggerStepThrough]
        public Label(uint offset, LabelConstant label)
        {
            Offset = offset;
            SourceLabel = label;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{SourceLabel.Name}:";
        }
    }
}