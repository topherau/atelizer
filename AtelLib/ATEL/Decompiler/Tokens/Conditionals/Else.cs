﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Conditionals
{
    public sealed class Else : IToken, IConditionalToken, IBlockToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Children.LastOrDefault()?.EndOffset ?? Offset;
        public IToken Condition { get; set; }
        public List<IToken> Children { get; set; }
        public Else NextElse { get; set; }

        public Else(uint offset, IToken condition, IEnumerable<IToken> children, Else nextElse = null)
        {
            Offset = offset;
            Condition = condition;
            Children = children.ToList();
            NextElse = nextElse;
        }

        public override string ToString()
        {
            if (Condition == null)
                return "else";

            return $"else if ({Condition})";
        }
    }
}