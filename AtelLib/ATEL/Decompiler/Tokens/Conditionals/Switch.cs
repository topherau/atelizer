﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Conditionals
{
    public sealed class Switch : IToken, IConditionalToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset
        {
            get
            {
                var last = SwitchBlock.LastOrDefault();
                return last?.Offset ?? Offset;
            }
        }

        public string Name { get; set; }
        public IToken Condition { get; set; }
        public List<Case> Cases { get; set; }
        public List<Classes.Instruction> SwitchBlock { get; set; }

        public Switch(uint offset, IToken condition, IEnumerable<Case> cases, List<Classes.Instruction> switchBlock)
        {
            Offset = offset;
            Condition = condition;
            Cases = cases.ToList();
            SwitchBlock = switchBlock;
        }


        public override string ToString()
        {
            return $"switch ({Condition})";
        }
    }
}