﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Conditionals
{
    public class While : IToken, IConditionalToken, IBlockToken
    {
        public uint Offset{ get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Children.LastOrDefault()?.EndOffset ?? Offset;
        public IToken Condition { get; set; }
        public List<IToken> Children { get; set; }

        public While(uint offset, IToken condition, IEnumerable<IToken> children)
        {
            Offset = offset;
            Condition = condition;
            Children = children.ToList();
        }
    }
}