﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Conditionals
{
    public sealed class If : IToken, IConditionalToken, IBlockToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => NextElse?.EndOffset ?? Children.LastOrDefault()?.EndOffset ?? Offset;
        public IToken Condition { get; set; }
        public List<IToken> Children { get; set; }
        public Else NextElse { get; set; }

        public If(uint offset, IToken condition, IEnumerable<IToken> children, Else nextElse = null)
        {
            Offset = offset;
            Condition = condition;
            Children = children.ToList();
            NextElse = nextElse;
        }

        public override string ToString()
        {
            return $"if ({Condition})";
        }
    }
}