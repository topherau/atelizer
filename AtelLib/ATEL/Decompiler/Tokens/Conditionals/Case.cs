﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens.Conditionals
{
    public sealed class Case : IToken, IConditionalToken, IBlockToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Children.LastOrDefault()?.EndOffset ?? Offset;
        public IToken Condition { get; set; }
        public LabelConstant StartLabel { get; set; }
        public List<IToken> Children { get; set; }

        public Case(uint offset, LabelConstant startLabel, IToken condition, IEnumerable<IToken> children)
        {
            Offset = offset;
            StartLabel = startLabel;
            Condition = condition;
            Children = children?.ToList();
        }

        public override string ToString()
        {
            switch (Condition)
            {
                case null:
                    return "default:";
                case Operation operation:
                    return $"case {operation.LeftOperand}:";
                default:
                    return $"case {Condition}:";
            }
        }

    }
}