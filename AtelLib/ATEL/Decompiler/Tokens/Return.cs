﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class Return : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;

        [DebuggerStepThrough]
        public Return(uint offset)
        {
            Offset = offset;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return "return";
        }
    }
}