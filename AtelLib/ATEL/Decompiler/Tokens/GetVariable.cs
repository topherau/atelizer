﻿using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class GetVariable : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public VariableConstant Variable { get; set; }
        public IToken Index { get; }

        [DebuggerStepThrough]
        public GetVariable(uint offset, VariableConstant variable, IToken index = null)
        {
            Offset = offset;
            Variable = variable;
            Index = index;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            if (Index != null)
            {
                return $"{Variable.Name}[{Index}";
            }
            else
            {
                return $"{Variable.Name}";
            }
        }
    }
}