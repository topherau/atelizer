﻿using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Values
{
    public class IntValue : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public int Value { get; set; }

        public IntValue(uint offset, int value)
        {
            Offset = offset;
            Value = value;
        }
    }
}