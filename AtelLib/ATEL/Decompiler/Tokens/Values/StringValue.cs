﻿using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Values
{
    public class StringValue : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public string Value { get; set; }

        public StringValue(uint offset, string value)
        {
            Offset = offset;
            Value = value;
        }
    }
}