﻿using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens.Values
{
    public class FloatValue : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public float Value { get; set; }

        public FloatValue(uint offset, float value)
        {
            Offset = offset;
            Value = value;
        }
    }
}