﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class GetConstantFloat : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public FloatConstant Constant { get; set; }
        public uint EndOffset => Offset;

        [DebuggerStepThrough]
        public GetConstantFloat(uint offset, FloatConstant constant)
        {
            Offset = offset;
            Constant = constant;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{Constant.Value}";
        }
    }
}