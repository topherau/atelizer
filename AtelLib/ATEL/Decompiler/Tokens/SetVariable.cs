﻿using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class SetVariable : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Value.StartOffset;
        public uint EndOffset => Offset;
        public VariableConstant Variable { get; set; }
        public IToken Value { get; set; }
        public IToken Index { get; }

        [DebuggerStepThrough]
        public SetVariable(uint offset, VariableConstant variable, IToken value, IToken index = null)
        {
            Offset = offset;
            Variable = variable;
            Value = value;
            Index = index;
        }
    }
}