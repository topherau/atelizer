﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class Break : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;

        [DebuggerStepThrough]
        public Break(uint offset)
        {
            Offset = offset;
        }
    }
}