﻿using System;
using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class SetRegister : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public RegisterType Register { get; set; }
        public IToken Value { get; set; }

        [DebuggerStepThrough]
        public SetRegister(uint offset, RegisterType register, IToken value)
        {
            Offset = offset;
            Register = register;
            Value = value;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{Register.ToString().ToLower()} = {Value}";
        }
    }
}