﻿using System.Diagnostics;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class GetConstantInt : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public IntConstant Constant { get; set; }

        [DebuggerStepThrough]
        public GetConstantInt(uint offset, IntConstant constant)
        {
            Offset = offset;
            Constant = constant;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{Constant.Value}";
        }
    }
}