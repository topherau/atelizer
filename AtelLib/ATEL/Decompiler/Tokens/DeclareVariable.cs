﻿using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class DeclareVariable : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset { get; }
        public uint EndOffset { get; }
        public VariableConstant Variable { get; set; }

        public DeclareVariable(VariableConstant variable)
        {
            Variable = variable;
        }
    }
}