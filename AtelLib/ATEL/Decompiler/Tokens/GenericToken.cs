﻿using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class GenericToken : IToken, INamedToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;

        public string Name { get; set; }

        [DebuggerStepThrough]
        public GenericToken(uint offset)
        {
            Offset = offset;
        }

        [DebuggerStepThrough]
        public GenericToken(uint offset, string name)
        {
            Offset = offset;
            Name = name;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}