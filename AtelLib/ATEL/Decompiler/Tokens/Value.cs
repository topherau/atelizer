﻿using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class Value : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public ValueType Type { get; set; }
        public string StringValue { get; set; }
        public short IntValue { get; set; }
        public float FloatValue { get; set; }

        [DebuggerStepThrough]
        public Value(uint offset, string stringValue)
        {
            Type = ValueType.String;
            Offset = offset;
            StringValue = stringValue;
        }

        [DebuggerStepThrough]
        public Value(uint offset, short intValue)
        {
            Type = ValueType.Int;
            Offset = offset;
            IntValue = intValue;
        }

        [DebuggerStepThrough]
        public Value(uint offset, float floatValue)
        {
            Type = ValueType.Float;
            Offset = offset;
            FloatValue = floatValue;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            switch (Type)
            {
                case ValueType.String:
                    return $"{StringValue}";
                case ValueType.Int:
                    return $"{IntValue}";
                case ValueType.Float:
                    return $"{FloatValue}";
            }

            return base.ToString();
        }
    }
}