﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class Function : IToken, INamedToken, IBlockToken
    {
        public uint Offset { get; set; }
        public string Name { get; set; }
        public List<IToken> Children { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;

        [DebuggerStepThrough]
        public Function(uint offset, string name, IEnumerable<IToken> children)
        {
            Offset = offset;
            Name = name;
            Children = children.ToList();
        }
    }
}