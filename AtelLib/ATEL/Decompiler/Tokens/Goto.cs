﻿using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class Goto : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public LabelConstant Destination { get; set; }

        [DebuggerStepThrough]
        public Goto(uint offset, LabelConstant destination)
        {
            Offset = offset;
            Destination = destination;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"goto {Destination.Name}";
        }
    }
}