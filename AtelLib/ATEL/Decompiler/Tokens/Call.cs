﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AtelLib.ATEL.Database.Classes;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public sealed class Call : IToken, INamedToken
    {
        public uint Offset { get; set; }
        public string Name { get; set; }
        public bool IsUnknown { get; set; }
        public AtelFunction Function { get; }
        public List<IToken> Arguments { get; }
        public uint StartOffset => Arguments?.FirstOrDefault()?.StartOffset ?? Offset;
        public uint EndOffset => Offset;

        [DebuggerStepThrough]
        public Call(uint offset, AtelFunction function, IEnumerable<IToken> arguments)
        {
            Offset = offset;
            Function = function;
            Arguments = arguments.ToList();
            Name = function.Name;
        }

        [DebuggerStepThrough]
        public Call(uint offset, string name)
        {
            Offset = offset;
            Name = name;
            IsUnknown = true;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            if (Function == null)
            {
                return $"{Name}()";
            }

            return Arguments?.Count > 0 
                ? $"{Function.Name}({string.Join(", ", Arguments)})" 
                : $"{Function.Name}()";
        }
    }
}