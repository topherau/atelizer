﻿using System;
using System.Diagnostics;
using System.Windows;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace AtelLib.ATEL.Decompiler.Tokens
{
    public class GetRegister : IToken
    {
        public uint Offset { get; set; }
        public uint StartOffset => Offset;
        public uint EndOffset => Offset;
        public RegisterType Register { get; set; }

        [DebuggerStepThrough]
        public GetRegister(uint offset, RegisterType register)
        {
            Offset = offset;
            Register = register;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return $"{Enum.GetName(typeof(RegisterType), Register)}";
        }
    }
}