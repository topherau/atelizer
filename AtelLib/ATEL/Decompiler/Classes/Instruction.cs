﻿using System.Diagnostics;
using System.Text;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Decompiler.Classes
{
    public class Instruction
    {
        public uint Offset { get; }
        public OpcodeType Opcode { get; }
        public short Operand { get; }

        [DebuggerStepThrough]
        public Instruction(uint offset, OpcodeType opcode, short operand = 0)
        {
            Offset = offset;
            Opcode = opcode;
            Operand = operand;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            var opcode = Disassembler.Classes.Opcode.Find(Opcode);
            if (opcode == null)
                return $"{Opcode} {Operand}";

            var opcodeBuilder = new StringBuilder(opcode.Mnemonic);
            switch (opcode.Operand)
            {
                case OperandType.None:
                    return opcodeBuilder.ToString();
                default:
                    opcodeBuilder.Append($" {Operand}");
                    return opcodeBuilder.ToString();
            }
        }
    }
}