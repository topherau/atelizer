﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AtelLib.ATEL.Decompiler.Classes
{
    public class InstructionList : List<Instruction>
    {
        public InstructionList(IDictionary<uint, Disassembler.Classes.Instruction> instructions)
        {
            // Create the instruction list from dictionary, ordered by offset
            AddRange(instructions.OrderBy(i => i.Key)
                .Select(i => new Instruction(i.Key, i.Value.Opcode, i.Value.Operand)));
        }

        [DebuggerStepThrough]
        public int FindPosition(uint offset)
        {
            return FindIndex(i => i.Offset == offset);
        }

        public new Instruction this[int position] => base[position];
        public Instruction this[uint offset] => this.FirstOrDefault(i => i.Offset == offset);
    }
}