﻿namespace AtelLib.ATEL.Database.Enums
{
    public enum GameType : int
    {
        None,
        FFX,
        FFX2
    }
}