﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AtelLib.ATEL.Database.Classes
{
    public class AtelLookupItem
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [ForeignKey(typeof(AtelLookup))]
        public int LookupId { get; set; }

        public int Value { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [ManyToOne]
        public AtelLookup Lookup { get; set; }
    }
}