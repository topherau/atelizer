﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Database.Enums;
using AtelLib.ATEL.Decompiler.Enums;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AtelLib.ATEL.Database.Classes
{
    public class AtelFunction
    {
        [PrimaryKey, AutoIncrement] public int Id { get; set; }
        public GameType Game { get; set; }
        public ushort FunctionId { get; set; }
        public string Name { get; set; }
        public AtelType Returns { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<AtelArgument> Arguments { get; set; }

        public AtelFunction()
        {
        }

        public AtelFunction(ushort functionId, string name, AtelType returns, IEnumerable<AtelArgument> arguments)
        {
            FunctionId = functionId;
            Name = name;
            Returns = returns;
            Arguments = arguments.ToList();
        }
    }
}