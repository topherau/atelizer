﻿using AtelLib.ATEL.Database.Enums;
using AtelLib.ATEL.Decompiler.Enums;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AtelLib.ATEL.Database.Classes
{
    public class AtelArgument
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [ForeignKey(typeof(AtelFunction))]
        public int FunctionId { get; set; }

        public int Order { get; set; }
        public string Name { get; set; }
        public AtelType Type { get; set; }
        public int Lookup { get; set; }
        public bool ShowAsHex { get; set; }

        [ManyToOne]
        public AtelFunction Function { get; set; }

        public AtelArgument()
        {

        }

        public AtelArgument(string name, AtelType type, int lookup = 0)
        {
            Name = name;
            Type = type;
            Lookup = lookup;
        }
    }
}