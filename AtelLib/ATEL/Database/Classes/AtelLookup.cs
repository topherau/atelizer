﻿using System.Collections.Generic;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AtelLib.ATEL.Database.Classes
{
    public class AtelLookup
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }

        [OneToMany(CascadeOperations=CascadeOperation.All)]
        public List<AtelLookupItem> Items { get; set; }
    }
}