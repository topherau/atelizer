﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using AtelLib.ATEL.Database.Classes;
using AtelLib.ATEL.Database.Enums;
using AtelLib.ATEL.Decompiler.Enums;
using SQLite.Net;
using SQLite.Net.Platform.Win32;
using SQLiteNetExtensions.Extensions;

namespace AtelLib.ATEL.Database
{
    public static class AtelDatabase
    {
        private static TableQuery<AtelFunction> _functions;
        private static SQLiteConnection _db;
        private static TableQuery<AtelLookup> _lookups;

        public static void Load(string databaseFile)
        {
            _db = new SQLiteConnection(new SQLitePlatformWin32(), "atelide.db");
            _db.CreateTable<AtelFunction>();
            _db.CreateTable<AtelArgument>();
            _db.CreateTable<AtelLookup>();
            _db.CreateTable<AtelLookupItem>();

            _functions = _db.Table<AtelFunction>();
            _lookups = _db.Table<AtelLookup>();

        }

        public static void Install()
        {
            if (!File.Exists("atelcam.ath"))
                return;

            InstallFromFile("atelcam.ath");

            if (!File.Exists("btlatel.ath"))
                return;

            InstallFromFile("btlatel.ath");

            AddFunction(GameType.FFX2, 0x5F, "halt", AtelType.None, new AtelArgument[0]);
            AddFunction(GameType.FFX2, 0xA9, "random", AtelType.Integer, new AtelArgument[0]);
        }

        private static void InstallFromFile(string file)
        {
            var camLines = File.ReadAllLines(file).ToList();
            camLines.RemoveAll(l => Regex.IsMatch(l, "\\s*#.*")); // Remove comment lines

            var functionId = 0;
            _db.BeginTransaction();
            foreach (var line in camLines)
            {
                if (functionId == 0)
                {
                    var fsMatch = Regex.Match(line, "\\s*funcspace\\s+(\\d+)");
                    if (!fsMatch.Success)
                        continue;
                    var parsedFuncSpace = int.TryParse(fsMatch.Groups[1].ToString(), out var funcSpace);
                    if (!parsedFuncSpace)
                        break;

                    functionId = funcSpace * 0x1000;
                    continue;
                }

                var fcMatch = Regex.Match(line,
                    "(int|float+\\*?)\\s+(\\w+)\\(\\s*((?:(?:int|float)(?: \\*)?[,\\s]*)*)\\s*\\);");
                if (!fcMatch.Success)
                    break;

                var returnText = fcMatch.Groups[1].Value.Replace(" ", "");
                var nameText = fcMatch.Groups[2].Value;
                var argsText = fcMatch.Groups[3].Value.Split(',').Select(a => a.Replace(" ", ""));

                AddFunction(GameType.FFX2, (ushort) functionId, nameText, ParseType(returnText),
                    argsText.Where(a => !string.IsNullOrEmpty(a)).Select(a => new AtelArgument(null, ParseType(a)))
                        .ToList());

                functionId++;
            }

            _db.Commit();
        }

        public static AtelType ParseType(string type)
        {
            switch (type)
            {
                case "int":
                    return AtelType.Integer;
                case "int*":
                    return AtelType.IntPtr;
                case "float":
                    return AtelType.Float;
                case "float*":
                    return AtelType.FloatPtr;
                default:
                    throw new Exception($"Unknown type {type}");
            }
        }

        public static void AddFunction(GameType gameType, ushort functionId, string name, AtelType returns,
            IEnumerable<AtelArgument> arguments)
        {
            var args = arguments.Select(a => new AtelArgument(a.Name, a.Type, a.Lookup)).ToList();

            var function = new AtelFunction
            {
                Game = gameType,
                FunctionId = functionId,
                Name = name,
                Returns = returns,
                Arguments = args
            };

            _db.InsertWithChildren(function);
        }

        public static AtelFunction FindFunction(int functionId)
        {
            var function = _functions.FirstOrDefault(f => f.FunctionId == functionId);

            return function == null ? null : _db.GetWithChildren<AtelFunction>(function.Id);
        }

        public static List<AtelFunction> GetFunctions(GameType gameType)
        {
            var functions = _functions.Where(f => f.Game == gameType).ToList();
            foreach (var function in functions)
                _db.GetChildren(function);
            return functions;
        }

        public static Dictionary<int, string> GetLookups()
        {
            var lookups = _lookups.ToDictionary(l => l.Id, l => l.Name);
            return lookups;
        }

        public static void AddValue(AtelLookup lookup, int value, string name)
        {
            var item = new AtelLookupItem
            {
                Name = name,
                Value = value,
                Lookup = lookup
            };
            _db.Insert(item);
            lookup.Items.Add(item);
            _db.UpdateWithChildren(lookup);
            _db.Commit();
        }

        public static void AddLookup(AtelLookup lookup)
        {
            _db.InsertWithChildren(lookup);
            _db.Commit();
        }

        public static AtelLookup GetLookup(int lookupId)
        {
            return _db.FindWithChildren<AtelLookup>(lookupId);
        }
    }
}