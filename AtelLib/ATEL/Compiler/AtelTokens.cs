﻿namespace AtelLib.ATEL.Compiler
{
    public enum AtelTokens
    {
        Undefined,
        Integer,
        Float,
        String,
        OpenParenthesis,
        CloseParenthesis,
        OpenCurlyBracket,
        CloseCurlyBracket,
        OpenSquareBracket,
        CloseSquareBracket,
        If,
        Else,
        While,
        Case,
        Switch,
        Goto,
        Ret
    }
}