﻿using System.IO;
using System.Text;

namespace AtelLib.ATEL.Compiler
{
    public class AtelCompiler
    {
        private string _source;
        private StringReader _reader;

        public AtelCompiler(string inputFile)
        {
            _source = File.ReadAllText(inputFile);
            _reader = new StringReader(_source);

            Compile();
        }

        private void Compile()
        {
            while (true)
            {
            }
        }

        private string ReadToken()
        {
            var token = new StringBuilder();
            while (true)
            {
                var next = (char) _reader.Read();
                switch (next)
                {
                    case '\t':
                    case ' ':
                    case '\n':
                    case '\r':
                    case '{':
                    case '}':
                        goto endRead;
                    default:
                        token.Append(next);
                        continue;
                }
            }

            endRead:

            return token.ToString();
        }
    }
}