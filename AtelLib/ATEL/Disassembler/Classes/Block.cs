﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Disassembler.Classes.Constants;
using AtelLib.ATEL.Disassembler.Classes.Descriptors;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Disassembler.Classes
{
    public class Block
    {
        public BlockDescriptor Descriptor { get; set; }
        public byte[] Bytes { get; set; }

        public List<VariableConstant> Variables { get; set; }
        public List<IntConstant> Integers { get; set; }
        public List<FloatConstant> Floats { get; set; }
        public List<LabelConstant> Labels { get; set; }
        public List<FunctionConstant> Functions { get; set; }

        private readonly Dictionary<uint, Dictionary<uint, Instruction>> _offsetInstructions =
            new Dictionary<uint, Dictionary<uint, Instruction>>();

        public Block(BlockDescriptor descriptor)
        {
            Descriptor = descriptor;
            Bytes = descriptor.Script;

            DisassembleHeader();
            DisassembleScript();
        }

        private void DisassembleHeader()
        {
            DisassembleVariables();
            DisassembleConstants();
            DisassembleFunctions();
        }

        private void DisassembleVariables()
        {
            var bits = 0;
            var chars = 0;
            var floats = 0;
            var ints = 0;
            var unks = 0;

            Variables = new List<VariableConstant>(Descriptor.Variables.Count);
            
            for (var v = 0; v < Descriptor.Variables.Capacity; v++)
            {
                var vb = Descriptor.Variables[v];
                string name;

                // Select initial variable name by type
                switch (vb.Type)
                {
                    case VariableType.Bit:
                        name = $"bit_{bits++}";
                        break;
                    case VariableType.Char:
                        name = $"char_{chars++}";
                        break;
                    case VariableType.Int:
                        name = $"int_{ints++}";
                        break;
                    case VariableType.Float:
                        name = $"float_{floats++}";
                        break;
                    default:
                        name = $"unk_{unks++}";
                        break;
                }

                Variables.Add(new VariableConstant(name, vb.Flags, vb.Type, vb.Offset, vb.Size));
            }
        }

        private void DisassembleConstants()
        {
            Integers = new List<IntConstant>(Descriptor.Integers.Count);
            for (var i = 0; i < Integers.Capacity; i++)
                Integers.Add(new IntConstant($"CONST_INT_{i}", Descriptor.Integers[i]));

            Floats = new List<FloatConstant>(Descriptor.Floats.Count);
            for (var f = 0; f < Floats.Capacity; f++)
                Floats.Add(new FloatConstant($"CONST_FLOAT_{f}", Descriptor.Floats[f]));
        }

        private void DisassembleFunctions()
        {
            Functions = new List<FunctionConstant>(Descriptor.Functions.Count);
            for (var f = 0; f < Functions.Capacity; f++)
                Functions.Add(new FunctionConstant($"function_{f}", Descriptor.Functions[f]));

            Labels = new List<LabelConstant>(Descriptor.Labels.Count);
            for (var l = 0; l < Labels.Capacity; l++)
                Labels.Add(new LabelConstant($"label_{l}", Descriptor.Labels[l]));
        }

        private void DisassembleScript()
        {
            foreach (var function in Functions)
            {
                // Find next function start, ignoring any functions that start at the same offset as this one
                var nextFunc = Functions.FirstOrDefault(f => f.Offset > function.Offset);

                // Calculate end offset, last function ends at end of script (obviously)
                var funcEnd = nextFunc == null
                    ? (uint) Bytes.Length
                    : nextFunc.Offset - Functions[0].Offset;

                // Multiple functions can start at the same offset, so check if we've already disassembled from this offset
                if (_offsetInstructions.ContainsKey(function.Offset))
                {
                    // Load previous disassembly
                    function.Instructions = _offsetInstructions[function.Offset];
                }
                else
                {
                    // Disassemble and save instructions, set on function object
                    var instructions = GetInstructions(function.Offset - Functions[0].Offset, funcEnd);
                    _offsetInstructions[function.Offset] = instructions;
                    function.Instructions = instructions;
                }
            }
        }

        private Dictionary<uint, Instruction> GetInstructions(uint start, uint end)
        {
            var functionStart = Functions[0].Offset;
            var instructions = new Dictionary<uint, Instruction>();
            for (var i = start; i < end; i++)
            {
                // Get opcode at offset
                var opcode = Bytes[i];

                // Lookup opcode info
                var opcodeInfo = Opcode.Find(opcode);
                if (opcodeInfo == null)
                    throw new Exception($"Found undefined opcode {opcode:X2} in at offset {i:X4}.");

                // Check if we need to read an operand from the array
                if (opcodeInfo.Operand == OperandType.None)
                {
                    // No operand required
                    instructions[i + functionStart] = new Instruction(opcodeInfo.Type);
                }
                else
                {
                    // Read operand from array into instruction
                    instructions[i + functionStart] =
                        new Instruction(opcodeInfo.Type, BitConverter.ToInt16(Bytes, (int) i + 1));
                    i += 2; // Advance position by 2 bytes
                }
            }

            return instructions;
        }
    }
}