﻿using AtelLib.ATEL.Disassembler.Interfaces;

namespace AtelLib.ATEL.Disassembler.Classes.Constants
{
    public class IntConstant : IConstant
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public IntConstant(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return $"{Name} ({Value})";
        }
    }
}