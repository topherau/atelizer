﻿using AtelLib.ATEL.Disassembler.Interfaces;

namespace AtelLib.ATEL.Disassembler.Classes.Constants
{
    public class LabelConstant : IConstant
    {
        public string Name { get; set; }
        public uint Offset { get; set; }
        public bool IsIgnored { get; set; }

        public LabelConstant(string name, uint offset)
        {
            Name = name;
            Offset = offset;
        }

        public override string ToString()
        {
            return $"{Name} ({Offset:X4})";
        }
    }
}