﻿using AtelLib.ATEL.Disassembler.Interfaces;

namespace AtelLib.ATEL.Disassembler.Classes.Constants
{
    public class FloatConstant : IConstant
    {
        public string Name { get; set; }
        public float Value { get; set; }

        public FloatConstant(string name, float value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return $"{Name} ({Value})";
        }
    }
}