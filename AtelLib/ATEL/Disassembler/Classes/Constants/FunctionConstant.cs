﻿using System.Collections.Generic;
using AtelLib.ATEL.Disassembler.Interfaces;

namespace AtelLib.ATEL.Disassembler.Classes.Constants
{
    public class FunctionConstant : IConstant
    {
        public string Name { get; set; }
        public uint Offset { get; set; }
        public Dictionary<uint, Instruction> Instructions { get;set; }

        public FunctionConstant(string name, uint offset)
        {
            Name = name;
            Offset = offset;
        }

        public override string ToString()
        {
            return $"{Name} ({Offset:X4})";
        }
    }
}