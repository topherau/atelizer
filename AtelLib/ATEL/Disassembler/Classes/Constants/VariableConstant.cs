﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using AtelLib.ATEL.Disassembler.Enums;
using AtelLib.ATEL.Disassembler.Interfaces;

namespace AtelLib.ATEL.Disassembler.Classes.Constants
{
    public class VariableConstant : IConstant, INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public VariableFlags Flags { get; set; }
        public VariableType Type { get; set; }
        public uint Offset { get; set; }
        public uint Length { get; set; }

        public bool IsArray => Length != 1;

        public VariableConstant(string name, VariableFlags flags, VariableType type, uint offset, uint length = 1)
        {
            Name = name;
            Flags = flags;
            Type = type;
            Offset = offset;
            Length = length;
        }
        
        public override string ToString()
        {
            return $"{Name}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}