﻿using System;
using System.Text;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Disassembler.Classes
{
    public class Instruction
    {
        public OpcodeType Opcode { get; set; }
        public short Operand { get; set; }

        public Instruction(OpcodeType opcode, short operand = 0)
        {
            Opcode = opcode;
            Operand = operand;
        }

        public override string ToString()
        {
            var opcode = Classes.Opcode.Find(Opcode);
            
            if (opcode.Operand == OperandType.None)
                return opcode.Mnemonic;

            switch (opcode.Operand)
            {
                case OperandType.Value:
                    return $"{opcode.Mnemonic} {Operand}";
                case OperandType.Variable:
                    return $"{opcode.Mnemonic} {Operand}";
                case OperandType.Integer:
                    return $"{opcode.Mnemonic} {Operand}";
                case OperandType.Float:
                    return $"{opcode.Mnemonic} {Operand}";
                case OperandType.Label:
                    return $"{opcode.Mnemonic} {Operand}";
                case OperandType.Call:
                    return $"{opcode.Mnemonic} {Operand:X4}";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}