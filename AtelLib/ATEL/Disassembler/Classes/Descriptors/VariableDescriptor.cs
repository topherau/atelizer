﻿using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Disassembler.Classes.Descriptors
{
    public class VariableDescriptor
    {
        public VariableType Type { get; set; }
        public VariableFlags Flags { get; set; }
        public uint Offset { get; set; }
        public uint Size { get; set; }
    }
}