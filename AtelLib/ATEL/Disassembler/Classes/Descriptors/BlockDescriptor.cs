﻿using System.Collections.Generic;

namespace AtelLib.ATEL.Disassembler.Classes.Descriptors
{
    public class BlockDescriptor
    {
        public List<string> Strings { get; set; }
        public List<VariableDescriptor> Variables { get; set; }
        public List<int> Integers { get; set; }
        public List<float> Floats { get; set; }
        public List<uint> Functions { get; set; }
        public List<uint> Labels { get; set; }
        public byte[] Script { get; set; }
    }
}