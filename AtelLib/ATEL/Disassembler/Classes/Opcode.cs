﻿using System.Collections.Generic;
using System.Linq;
using AtelLib.ATEL.Disassembler.Enums;

namespace AtelLib.ATEL.Disassembler.Classes
{
    public class Opcode
    {
        public OpcodeType Type { get; set; }
        public OperandType Operand { get; set; }
        public string Mnemonic { get; set; }

        public Opcode(OpcodeType type, OperandType operand, string mnemonic)
        {
            Type = type;
            Operand = operand;
            Mnemonic = mnemonic;
        }

        public bool IsReturn
        {
            get
            {
                switch (Type)
                {
                    case OpcodeType.RET:
                    case OpcodeType.RETT:
                    case OpcodeType.RETN:
                    case OpcodeType.RETTN:
                    case OpcodeType.DRET:
                    case OpcodeType.RTS:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsDirectJump
        {
            get
            {
                switch (Type)
                {
                    case OpcodeType.JMP:
                    case OpcodeType.JZ:
                    case OpcodeType.JNZ:
                    case OpcodeType.GOTO:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsIndirectJump
        {
            get
            {
                switch (Type)
                {
                    case OpcodeType.POPXJMP:
                    case OpcodeType.POPXCJMP:
                    case OpcodeType.POPXNCJMP:
                        return true;
                    default:
                        return false;
                }
            }
        }

        private static Dictionary<OpcodeType, Opcode> _opcodes = new Dictionary<OpcodeType, Opcode>();

        static Opcode()
        {
            AddOpcode(OpcodeType.OPNOP, OperandType.None, "nop");
            AddOpcode(OpcodeType.OPLOR, OperandType.None, "lor");
            AddOpcode(OpcodeType.OPLAND, OperandType.None, "OPLAND");
            AddOpcode(OpcodeType.OPOR, OperandType.None, "OPOR");
            AddOpcode(OpcodeType.OPEOR, OperandType.None, "OPEOR");
            AddOpcode(OpcodeType.OPAND, OperandType.None, "OPAND");
            AddOpcode(OpcodeType.OPEQ, OperandType.None, "OPEQ");
            AddOpcode(OpcodeType.OPNE, OperandType.None, "OPNE");
            AddOpcode(OpcodeType.OPGTU, OperandType.None, "OPGTU");
            AddOpcode(OpcodeType.OPLSU, OperandType.None, "OPLSU");
            AddOpcode(OpcodeType.OPGT, OperandType.None, "OPGT");
            AddOpcode(OpcodeType.OPLS, OperandType.None, "OPLS");
            AddOpcode(OpcodeType.OPGTEU, OperandType.None, "OPGTEU");
            AddOpcode(OpcodeType.OPLSEU, OperandType.None, "OPLSEU");
            AddOpcode(OpcodeType.OPGTE, OperandType.None, "OPGTE");
            AddOpcode(OpcodeType.OPLSE, OperandType.None, "OPLSE");
            AddOpcode(OpcodeType.OPBON, OperandType.None, "OPBON");
            AddOpcode(OpcodeType.OPBOFF, OperandType.None, "OPBOFF");
            AddOpcode(OpcodeType.OPSLL, OperandType.None, "OPSLL");
            AddOpcode(OpcodeType.OPSRL, OperandType.None, "OPSRL");
            AddOpcode(OpcodeType.OPADD, OperandType.None, "OPADD");
            AddOpcode(OpcodeType.OPSUB, OperandType.None, "OPSUB");
            AddOpcode(OpcodeType.OPMUL, OperandType.None, "OPMUL");
            AddOpcode(OpcodeType.OPDIV, OperandType.None, "OPDIV");
            AddOpcode(OpcodeType.OPMOD, OperandType.None, "OPMOD");
            AddOpcode(OpcodeType.OPNOT, OperandType.None, "OPNOT");
            AddOpcode(OpcodeType.OPUMINUS, OperandType.None, "OPUMINUS");
            AddOpcode(OpcodeType.OPFIXADRS, OperandType.None, "OPFIXADRS");
            AddOpcode(OpcodeType.OPBNOT, OperandType.None, "OPBNOT");
            AddOpcode(OpcodeType.LABEL, OperandType.None, "LABEL");
            AddOpcode(OpcodeType.TAG, OperandType.None, "tag");
            AddOpcode(OpcodeType.PUSHV, OperandType.Variable, "PUSHV");
            AddOpcode(OpcodeType.POPV, OperandType.Variable, "POPV");
            AddOpcode(OpcodeType.POPVL, OperandType.None, "POPVL");
            AddOpcode(OpcodeType.PUSHAR, OperandType.None, "PUSHAR");
            AddOpcode(OpcodeType.POPAR, OperandType.None, "POPAR");
            AddOpcode(OpcodeType.POPARL, OperandType.None, "POPARL");
            AddOpcode(OpcodeType.POPA, OperandType.None, "POPA");
            AddOpcode(OpcodeType.PUSHA, OperandType.None, "PUSHA");
            AddOpcode(OpcodeType.PUSHARP, OperandType.None, "PUSHARP");
            AddOpcode(OpcodeType.PUSHX, OperandType.None, "PUSHX");
            AddOpcode(OpcodeType.PUSHY, OperandType.None, "PUSHY");
            AddOpcode(OpcodeType.POPX, OperandType.None, "POPX");
            AddOpcode(OpcodeType.REPUSH, OperandType.None, "REPUSH");
            AddOpcode(OpcodeType.POPY, OperandType.None, "POPY");
            AddOpcode(OpcodeType.PUSHI, OperandType.None, "PUSHI");
            AddOpcode(OpcodeType.PUSHII, OperandType.None, "PUSHII");
            AddOpcode(OpcodeType.PUSHF, OperandType.None, "PUSHF");
            AddOpcode(OpcodeType.JMP, OperandType.None, "jmp");
            AddOpcode(OpcodeType.CJMP, OperandType.None, "CJMP");
            AddOpcode(OpcodeType.NCJMP, OperandType.None, "NCJMP");
            AddOpcode(OpcodeType.JSR, OperandType.None, "jsr");
            AddOpcode(OpcodeType.RTS, OperandType.None, "rts");
            AddOpcode(OpcodeType.CALL, OperandType.None, "CALL");
            AddOpcode(OpcodeType.REQ, OperandType.None, "req");
            AddOpcode(OpcodeType.REQSW, OperandType.None, "reqsw");
            AddOpcode(OpcodeType.REQEW, OperandType.None, "reqew");
            AddOpcode(OpcodeType.PREQ, OperandType.None, "PREQ");
            AddOpcode(OpcodeType.PREQSW, OperandType.None, "PREQSW");
            AddOpcode(OpcodeType.PREQEW, OperandType.None, "PREQEW");
            AddOpcode(OpcodeType.RET, OperandType.None, "ret");
            AddOpcode(OpcodeType.RETN, OperandType.None, "RETN");
            AddOpcode(OpcodeType.RETT, OperandType.None, "RETT");
            AddOpcode(OpcodeType.RETTN, OperandType.None, "RETTN");
            AddOpcode(OpcodeType.HALT, OperandType.None, "HALT");
            AddOpcode(OpcodeType.PUSHN, OperandType.None, "PUSHN");
            AddOpcode(OpcodeType.PUSHT, OperandType.None, "PUSHT");
            AddOpcode(OpcodeType.PUSHVP, OperandType.None, "PUSHVP");
            AddOpcode(OpcodeType.PUSHFIX, OperandType.None, "PUSHFIX");
            AddOpcode(OpcodeType.FREQ, OperandType.None, "FREQ");
            AddOpcode(OpcodeType.TREQ, OperandType.None, "TREQ");
            AddOpcode(OpcodeType.BREQ, OperandType.None, "BREQ");
            AddOpcode(OpcodeType.BFREQ, OperandType.None, "BFREQ");
            AddOpcode(OpcodeType.BTREQ, OperandType.None, "BTREQ");
            AddOpcode(OpcodeType.FREQSW, OperandType.None, "FREQSW");
            AddOpcode(OpcodeType.TREQSW, OperandType.None, "TREQSW");
            AddOpcode(OpcodeType.BREQSW, OperandType.None, "BREQSW");
            AddOpcode(OpcodeType.BFREQSW, OperandType.None, "BFREQSW");
            AddOpcode(OpcodeType.BTREQSW, OperandType.None, "BTREQSW");
            AddOpcode(OpcodeType.FREQEW, OperandType.None, "FREQEW");
            AddOpcode(OpcodeType.TREQEW, OperandType.None, "TREQEW");
            AddOpcode(OpcodeType.BREQEW, OperandType.None, "BREQEW");
            AddOpcode(OpcodeType.BFREQEW, OperandType.None, "BFREQEW");
            AddOpcode(OpcodeType.BTREQEW, OperandType.None, "BTREQEW");
            AddOpcode(OpcodeType.DRET, OperandType.None, "dret");
            AddOpcode(OpcodeType.POPXJMP, OperandType.None, "POPXJMP");
            AddOpcode(OpcodeType.POPXCJMP, OperandType.None, "POPXCJMP");
            AddOpcode(OpcodeType.POPXNCJMP, OperandType.None, "POPXNCJMP");
            AddOpcode(OpcodeType.CALLPOPA, OperandType.None, "CALLPOPA");
            AddOpcode(OpcodeType.POPI0, OperandType.None, "POPI0");
            AddOpcode(OpcodeType.POPI1, OperandType.None, "POPI1");
            AddOpcode(OpcodeType.POPI2, OperandType.None, "POPI2");
            AddOpcode(OpcodeType.POPI3, OperandType.None, "POPI3");
            AddOpcode(OpcodeType.POPF0, OperandType.None, "POPF0");
            AddOpcode(OpcodeType.POPF1, OperandType.None, "POPF1");
            AddOpcode(OpcodeType.POPF2, OperandType.None, "POPF2");
            AddOpcode(OpcodeType.POPF3, OperandType.None, "POPF3");
            AddOpcode(OpcodeType.POPF4, OperandType.None, "POPF4");
            AddOpcode(OpcodeType.POPF5, OperandType.None, "POPF5");
            AddOpcode(OpcodeType.POPF6, OperandType.None, "POPF6");
            AddOpcode(OpcodeType.POPF7, OperandType.None, "POPF7");
            AddOpcode(OpcodeType.POPF8, OperandType.None, "POPF8");
            AddOpcode(OpcodeType.POPF9, OperandType.None, "POPF9");
            AddOpcode(OpcodeType.PUSHI0, OperandType.None, "PUSHI0");
            AddOpcode(OpcodeType.PUSHI1, OperandType.None, "PUSHI1");
            AddOpcode(OpcodeType.PUSHI2, OperandType.None, "PUSHI2");
            AddOpcode(OpcodeType.PUSHI3, OperandType.None, "PUSHI3");
            AddOpcode(OpcodeType.PUSHF0, OperandType.None, "PUSHF0");
            AddOpcode(OpcodeType.PUSHF1, OperandType.None, "PUSHF1");
            AddOpcode(OpcodeType.PUSHF2, OperandType.None, "PUSHF2");
            AddOpcode(OpcodeType.PUSHF3, OperandType.None, "PUSHF3");
            AddOpcode(OpcodeType.PUSHF4, OperandType.None, "PUSHF4");
            AddOpcode(OpcodeType.PUSHF5, OperandType.None, "PUSHF5");
            AddOpcode(OpcodeType.PUSHF6, OperandType.None, "PUSHF6");
            AddOpcode(OpcodeType.PUSHF7, OperandType.None, "PUSHF7");
            AddOpcode(OpcodeType.PUSHF8, OperandType.None, "PUSHF8");
            AddOpcode(OpcodeType.PUSHF9, OperandType.None, "PUSHF9");
            AddOpcode(OpcodeType.PUSHAINTER, OperandType.None, "PUSHAINTER");
            AddOpcode(OpcodeType.SYSTEM, OperandType.None, "system");
            AddOpcode(OpcodeType.REQWAIT, OperandType.None, "REQWAIT");
            AddOpcode(OpcodeType.PREQWAIT, OperandType.None, "PREQWAIT");
            AddOpcode(OpcodeType.REQCHG, OperandType.None, "REQCHG");
            AddOpcode(OpcodeType.ACTREQ, OperandType.None, "ACTREQ");
            AddOpcode(OpcodeType.VPUSH, OperandType.Variable, "vpush");
            AddOpcode(OpcodeType.VPOP, OperandType.Variable, "vpop");
            AddOpcode(OpcodeType.OPA1, OperandType.Value, "OPA1");
            AddOpcode(OpcodeType.ARRPUSH, OperandType.Variable, "arrpush");
            AddOpcode(OpcodeType.ARRPOP, OperandType.Variable, "arrpop");
            AddOpcode(OpcodeType.OPA7, OperandType.Integer, "OPA7");
            AddOpcode(OpcodeType.IPUSH, OperandType.Integer, "ipush");
            AddOpcode(OpcodeType.PUSH, OperandType.Value, "push");
            AddOpcode(OpcodeType.FPUSH, OperandType.Float, "fpush");
            AddOpcode(OpcodeType.GOTO, OperandType.Label, "goto");
            AddOpcode(OpcodeType.OPB3, OperandType.Call, "OPB3");
            AddOpcode(OpcodeType.EXEC, OperandType.Call, "exec");
            AddOpcode(OpcodeType.JNZ, OperandType.Label, "jnz");
            AddOpcode(OpcodeType.JZ, OperandType.Label, "jz");
            AddOpcode(OpcodeType.SUB, OperandType.Call, "sub");
        }

        public static Opcode Find(OpcodeType opcode)
        {
            return _opcodes.FirstOrDefault(o => o.Key == opcode).Value;
        }

        public static Opcode Find(byte opcode)
        {
            return _opcodes.FirstOrDefault(o => (byte) o.Key == opcode).Value;
        }

        private static void AddOpcode(OpcodeType opcode, OperandType operand, string name)
        {
            _opcodes[opcode] = new Opcode(opcode, operand, name);
        }
    }
}