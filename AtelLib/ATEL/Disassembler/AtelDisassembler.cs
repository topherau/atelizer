﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AtelLib.ATEL.Disassembler.Classes;
using AtelLib.ATEL.Disassembler.Classes.Descriptors;
using AtelLib.ATEL.Disassembler.Enums;
using AtelLib.ATEL.Enums;

namespace AtelLib.ATEL.Disassembler
{
    public class AtelDisassembler
    {
        public string Author { get; set; }
        public string Filename { get; set; }
        
        public List<BlockDescriptor> BlockDescriptors { get; set; }
        public List<Block> Blocks { get; set; }

        private readonly byte[] _bytes;
        private MemoryStream _stream;
        private BinaryReader _reader;

        private uint _scriptLength;
        private uint _dataLength;
        private ushort _blockCount;
        private uint _scriptStart;

        private uint[] _descriptorStarts;
        private uint[][] _typeStarts;
        private uint _unknownBlock3;
        private uint _offsetTable;
        private uint _unknownTable;

        public AtelDisassembler(byte[] bytes)
        {
            _bytes = bytes;
            _stream = new MemoryStream(_bytes);
            _reader = new BinaryReader(_stream);

            Read();
        }

        private void Read()
        {
            ReadHeader();
            ReadBlocks();
        }

        private void ReadHeader()
        {
            _scriptLength = _reader.ReadUInt32(); //		0x0000  Length of the script section
            var unknown_04 = _reader.ReadUInt16();
            var unknown_06 = _reader.ReadUInt16();
            var authorOffset = _reader.ReadUInt32(); //		0x0008  Author name offset
            var filenameOffset = _reader.ReadUInt32(); //	0x000C  Filename offset
            _dataLength = _reader.ReadUInt32(); //          0x0010  Full length of data from beginning of stream
            var unknown_14 = _reader.ReadUInt16();
            var unknown_16 = _reader.ReadUInt16();
            var unknown_18 = _reader.ReadUInt16();
            var unknown_1a = _reader.ReadUInt16();
            var unknown_1c = _reader.ReadUInt16();
            var unknown_1e = _reader.ReadUInt16();
            _unknownTable = _reader.ReadUInt32(); //	0x0020 unknown block offset
            var unknown_24 = _reader.ReadUInt32();
            _offsetTable = _reader.ReadUInt32(); //		0x0028 unknown block offset
            _unknownBlock3 = _reader.ReadUInt32(); //			Unknown offset
            _scriptStart = _reader.ReadUInt32(); //			Script section offset
            _blockCount = _reader.ReadUInt16(); //		Number of blocks again?
            var unknownCount_36 = _reader.ReadUInt16();
            
            Author = ReadString(authorOffset);
            Filename = ReadString(filenameOffset);
        }

        private void ReadBlocks()
        {
            _descriptorStarts = new uint[_blockCount];
            _typeStarts = new uint[_blockCount][];
            
            Blocks = new List<Block>();
            BlockDescriptors = new List<BlockDescriptor>(_blockCount);

            // Read offset for block descriptor
            for (var i = 0; i < _blockCount; i++)
                _descriptorStarts[i] = _reader.ReadUInt32();

            // Read block descriptor and data
            for (var i = 0; i < _blockCount; i++)
            {
                ReadDescriptor(i);
                ReadVariables(i);
                ReadInts(i);
                ReadFloats(i);
                ReadFunctions(i);
                ReadLabels(i);
            }

            // Read script bytes and create block
            for (var i = 0; i < _blockCount; i++)
            {
                ReadScriptBytes(i);
                Blocks.Add(new Block(BlockDescriptors[i]));
            }
        }

        private void ReadDescriptor(int blockId)
        {
            // The descriptor details how many of each variable and constant value
            // is stored in the script, and where each value is stored

            _stream.Seek(_descriptorStarts[blockId], SeekOrigin.Begin);

            BlockDescriptors.Add(new BlockDescriptor
            {
                Strings = new List<string>(_reader.ReadUInt16()),
                Variables = new List<VariableDescriptor>(_reader.ReadUInt16()),
                Integers = new List<int>(_reader.ReadUInt16()),
                Floats = new List<float>(_reader.ReadUInt16()),
                Functions = new List<uint>(_reader.ReadUInt16()),
                Labels = new List<uint>(_reader.ReadUInt16()),
            });

            var descriptorUnknown = _reader.ReadUInt32();

            _typeStarts[blockId] = new uint[6];
            for (var i = 0; i < 6; i++)
                _typeStarts[blockId][i] = _reader.ReadUInt32();

            var constBlockZero1 = _reader.ReadUInt32(); // always 0
            var constBlockZero2 = _reader.ReadUInt32(); // always 0

            var globalFloatTable = _reader.ReadUInt32();
        }

        private void ReadVariables(int blockId)
        {
            _stream.Seek(_typeStarts[blockId][(int) DataType.Variable], SeekOrigin.Begin);

            for (var i = 0; i < BlockDescriptors[blockId].Variables.Capacity; i++)
            {
                var offset = _reader.ReadUInt16();

                var unknown = _reader.ReadByte(); // always 0

                var type = _reader.ReadByte();
                var size = _reader.ReadUInt32();

                var varFlags = (VariableFlags) (type & 0x0F);
                var varType = (VariableType) ((type & 0xF0) >> 4);

                BlockDescriptors[blockId].Variables.Add(new VariableDescriptor
                {
                    Type = varType,
                    Flags = varFlags,
                    Size = size,
                    Offset = offset
                });
            }
        }

        private void ReadInts(int blockId)
        {
            _stream.Seek(_typeStarts[blockId][(int) DataType.Int], SeekOrigin.Begin);
            for (var i = 0; i < BlockDescriptors[blockId].Integers.Capacity; i++)
                BlockDescriptors[blockId].Integers.Add(_reader.ReadInt32());
        }

        private void ReadFloats(int blockId)
        {
            _stream.Seek(_typeStarts[blockId][(int) DataType.Float], SeekOrigin.Begin);
            for (var i = 0; i < BlockDescriptors[blockId].Floats.Capacity; i++)
                BlockDescriptors[blockId].Floats.Add(_reader.ReadSingle());
        }

        private void ReadFunctions(int blockId)
        {
            _stream.Seek(_typeStarts[blockId][(int) DataType.Function], SeekOrigin.Begin);
            for (var i = 0; i < BlockDescriptors[blockId].Functions.Capacity; i++)
                BlockDescriptors[blockId].Functions.Add(_reader.ReadUInt32());
        }

        private void ReadLabels(int blockId)
        {
            _stream.Seek(_typeStarts[blockId][(int) DataType.Label], SeekOrigin.Begin);
            for (var i = 0; i < BlockDescriptors[blockId].Labels.Capacity; i++)
                BlockDescriptors[blockId].Labels.Add(_reader.ReadUInt32());
        }

        private void ReadScriptBytes(int blockId)
        {
            // Retrieve the bytes that correspond to the instructions for this block
            var blockStart = BlockDescriptors[blockId].Functions[0];
            var blockEnd = blockId == BlockDescriptors.Capacity - 1
                ? _scriptLength //                      Last block in script, therefore block ends at end of script
                : BlockDescriptors[blockId + 1].Functions[0]; // Block ends at start of next block

            // Create array and copy 
            var scriptBytes = new byte[blockEnd - blockStart];
            _stream.Seek(_scriptStart + blockStart, SeekOrigin.Begin);
            _stream.Read(scriptBytes, 0, scriptBytes.Length);
            
            BlockDescriptors[blockId].Script = scriptBytes;
        }

        private string ReadString(uint offset)
        {
            // Read a null-terminated string of arbitrary length from the script buffer
            var sb = new StringBuilder();
            while (true)
            {
                if (offset >= _bytes.Length)
                    throw new Exception("Reached end of script without string terminator.");
                if (_bytes[offset] == 0)
                    break;
                sb.Append((char) _bytes[offset]);
                offset++;
            }

            return sb.ToString();
        }

        private void ReadUnknownBlock2()
        {
            //_stream.Seek(_offsetTable, SeekOrigin.Begin);
            //var blockData = new List<uint>();
            //while (true)
            //{
            //    var data = _reader.ReadUInt32();
            //    if (blockData.Count != 0 && data == 0)
            //        break;

            //    blockData.Add(data);
            //}
        }

        private void ReadUnknownBlock3()
        {
            _stream.Seek(_unknownBlock3, SeekOrigin.Begin);
            var blockLength = _reader.ReadUInt32();
            var blockData = new List<uint>();
            while (_stream.Position < _unknownTable)
            {
                blockData.Add(_reader.ReadUInt32());
            }

            var enums = new Dictionary<MetadataType, uint>();
            for (var i = 0; i < blockData.Count; i++)
            {
                if (Enum.IsDefined(typeof(MetadataType), i))
                {
                    enums[(MetadataType) i] = blockData[i];
                }
                else if (blockData[i] != 0)
                {
                    //throw new Exception("Unknown block data");
                }
            }

            var buildTime = ReadString(enums[MetadataType.BuildTime]);
        }
    }
}