﻿namespace AtelLib.ATEL.Disassembler.Interfaces
{
    public interface IConstant
    {
        string Name { get; set; }
    }
}