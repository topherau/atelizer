﻿namespace AtelLib.ATEL.Disassembler.Enums
{
    public enum OperandType
    {
        None,
        Value,
        Variable,
        Integer,
        Float,
        Label,
        Call
    }
}