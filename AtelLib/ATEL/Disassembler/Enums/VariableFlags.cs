﻿using System;

namespace AtelLib.ATEL.Disassembler.Enums
{
    [Flags]
    public enum VariableFlags : byte
    {
        Global = 0,
        Array = 1,
        Local = 2,
        Block = 4,
        Unknown = 8
    }
}