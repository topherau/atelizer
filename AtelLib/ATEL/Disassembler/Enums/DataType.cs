﻿namespace AtelLib.ATEL.Disassembler.Enums
{
    public enum DataType
    {
        String,
        Variable,
        Int,
        Float,
        Function,
        Label
    }
}