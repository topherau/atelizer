﻿namespace AtelLib.ATEL.Disassembler.Enums
{
    public enum VariableType
    {
        Bit,
        Char,
        Unknown2,
        Unknown3,
        Unknown4,
        Int,
        Float
    }
}