﻿using System.IO;
using System.Linq;
using AtelLib.ATEL.Enums;

namespace AtelLib.ATEL
{
    public class AtelReader
    {
        private static readonly byte[] FfxMonsterFileHeader = {8, 0, 0, 0};
        private static readonly byte[] Ffx2MonsterFileHeader = { 9, 0, 0, 0 };
        private static readonly byte[] FfxEventFileHeader = { 69, 86, 48, 49 }; // "EV01"

        private readonly string _fileName;
        private readonly Stream _stream;
        private readonly BinaryReader _reader;

        private byte[] _script;

        public byte[] GetScriptBytes() => (byte[])_script.Clone();
        public AtelFileType FileType { get; private set; }

        public AtelReader(string fileName)
        {
            _fileName = fileName;
            _stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            _reader = new BinaryReader(_stream);

            Read();
        }

        public AtelReader(byte[] bytes)
        {
            _fileName = null;
            _stream = new MemoryStream(bytes);
            _reader = new BinaryReader(_stream);

            Read();
        }

        private void Read()
        {
            var header = _reader.ReadBytes(4);
            if (header.SequenceEqual(FfxEventFileHeader))
            {
                LoadFfxEventFile();
            }
            else if (header.SequenceEqual(FfxMonsterFileHeader))
            {
                LoadFfxMonsterFile();
            }
            else if (header.SequenceEqual(Ffx2MonsterFileHeader))
            {
                LoadFfx2MonsterFile();
            }
            else
            {
                LoadBinaryFile();
            }
        }

        private void LoadFfxEventFile()
        {
            // Read segment 0 offset and segment 1 offset
            var seg0 = _reader.ReadUInt32();
            var seg1 = _reader.ReadUInt32();
            while (seg1 == 0)
                seg1 = _reader.ReadUInt32();

            var segLength = seg1 - seg0;
            _stream.Seek(seg0, SeekOrigin.Begin);
            _script = _reader.ReadBytes((int)segLength);
            FileType = AtelFileType.FfxEvent;
        }

        private void LoadFfxMonsterFile()
        {
            // Read segment 0 offset and segment 1 offset
            var seg0 = _reader.ReadUInt32();
            var seg1 = _reader.ReadUInt32();
            while (seg1 == 0)
                seg1 = _reader.ReadUInt32();

            var segLength = seg1 - seg0;
            _stream.Seek(seg0, SeekOrigin.Begin);
            _script = _reader.ReadBytes((int)segLength);
            FileType = AtelFileType.FfxMonster;
        }

        private void LoadFfx2MonsterFile()
        {
            // Read segment 0 offset and segment 1 offset
            var seg0 = _reader.ReadUInt32();
            var seg1 = _reader.ReadUInt32();
            while (seg1 == 0)
                seg1 = _reader.ReadUInt32();

            var segLength = seg1 - seg0;
            _stream.Seek(seg0, SeekOrigin.Begin);
            _script = _reader.ReadBytes((int)segLength);
            FileType = AtelFileType.Ffx2Monster;
        }

        private void LoadBinaryFile()
        {
            // Read entire file into script buffer
            _stream.Seek(0, SeekOrigin.Begin);
            _script = _reader.ReadBytes((int)_stream.Length);
            FileType = AtelFileType.None;
        }
    }
}