﻿namespace AtelLib.ATEL.Enums
{
    public enum AtelFileType
    {
        None,
        FfxMonster,
        FfxEvent,
        Ffx2Monster,
        Ffx2Event
    }
}