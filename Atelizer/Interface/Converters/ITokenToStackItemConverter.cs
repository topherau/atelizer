﻿using System;
using System.Globalization;
using System.Windows.Data;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Converters
{
    public class ITokenToStackItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is IToken token))
                return "Unknown";

            switch (token)
            {
                default:
                    return token.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}