﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace Atelizer.Interface.Converters
{
    public class EnumToDictionaryConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (!(value is Type enumType))
                return null;
            
            if (!enumType.IsEnum)
                return null;

            var enumValues = Enum.GetValues(enumType);
            var enumNames = Enum.GetNames(enumType);
            var enumDict = new Dictionary<object, string>();

            var startValue = 0;
            if(bool.TryParse((string)parameter, out var ignoreZero))
                if (ignoreZero)
                    startValue = 1;

            for (var i = startValue; i < enumValues.Length; i++)
            {
                var enumValue = enumValues.GetValue(i);
                // Check if enum has description, i.e. long name
                var enumField = enumType.GetField(enumValue.ToString());
                var attributes =
                    (DescriptionAttribute[])enumField.GetCustomAttributes(
                        typeof(DescriptionAttribute),
                        false);

                if (attributes.Length > 0)
                    enumDict[enumValue] = attributes[0].Description; // Name from description
                else
                    enumDict[enumValue] = (string)enumNames.GetValue(i); // Name from enum

            }

            return enumDict;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}