﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Atelizer.Interface.Elements;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;
using AtelLib.ATEL.Decompiler.Tokens.Values;
using AtelLib.ATEL.Disassembler.Classes.Constants;
using Label = AtelLib.ATEL.Decompiler.Tokens.Label;

namespace Atelizer.Interface
{
    public static class TokenExtension
    {
        public static FrameworkElement ToFrameworkElement(this IToken token)
        {
            switch (token)
            {
                case Break breakToken:
                    return breakToken.ToFrameworkElement();
                case Case caseToken:
                    return caseToken.ToFrameworkElement();
                case Else elseToken:
                    return elseToken.ToFrameworkElement();
                case If ifToken:
                    return ifToken.ToFrameworkElement();
                case Switch switchToken:
                    return switchToken.ToFrameworkElement();
                case While whileToken:
                    return whileToken.ToFrameworkElement();
                case Call callToken:
                    return callToken.ToFrameworkElement();
                case DeclareVariable declareVariableToken:
                    return declareVariableToken.ToFrameworkElement();
                case GenericToken genericTokenToken:
                    return genericTokenToken.ToFrameworkElement();
                case GetConstantInt getConstantFloatToken:
                    return getConstantFloatToken.ToFrameworkElement();
                case GetConstantFloat getConstantIntToken:
                    return getConstantIntToken.ToFrameworkElement();
                case GetRegister getRegisterToken:
                    return getRegisterToken.ToFrameworkElement();
                case GetVariable getVariableToken:
                    return getVariableToken.ToFrameworkElement();
                case Goto gotoToken:
                    return gotoToken.ToFrameworkElement();
                case IntValue intToken:
                    return intToken.ToFrameworkElement();
                case FloatValue floatToken:
                    return floatToken.ToFrameworkElement();
                case StringValue stringToken:
                    return stringToken.ToFrameworkElement();
                case Label labelToken:
                    return labelToken.ToFrameworkElement();
                case Operation operationToken:
                    return operationToken.ToFrameworkElement();
                case Return returnToken:
                    return returnToken.ToFrameworkElement();
                case SetRegister setRegisterToken:
                    return setRegisterToken.ToFrameworkElement();
                case SetVariable setVariableToken:
                    return setVariableToken.ToFrameworkElement();
                case Value valueToken:
                    return valueToken.ToFrameworkElement();
                default:
                    return new TextBlock
                    {
                        Text = $"????"
                    };
            }
        }

        public static FrameworkElement ToFrameworkElement(this Break token)
        {
            return new KeywordElement("break", token);
        }

        public static FrameworkElement ToFrameworkElement(this Case token)
        {
            var labelPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Tag = token
            };

            switch (token.Condition)
            {
                case null:
                    labelPanel.Children.Add(new KeywordElement("default", token));
                    break;
                case Operation operation:
                    labelPanel.Children.Add(new KeywordElement("case ", token));
                    if (operation.Type != OperatorType.EQ)
                        labelPanel.Children.Add(new OperatorElement(operation.GetOperatorString(), token));
                    labelPanel.Children.Add(operation.RightOperand.ToFrameworkElement());
                    break;
                default:
                    labelPanel.Children.Add(new KeywordElement("case ", token));
                    labelPanel.Children.Add(token.Condition.ToFrameworkElement());
                    break;
            }

            labelPanel.Children.Add(new OperatorElement(":", token));

            return labelPanel;
        }

        public static FrameworkElement ToFrameworkElement(this Else token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new TextBlock
                    {
                        Text = "else",
                        Foreground = SyntaxBrushes.Keyword
                    }
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this If token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new KeywordElement("if ", token),
                    new OperatorElement("(", token),
                    token.Condition.ToFrameworkElement(),
                    new OperatorElement(")", token),
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this Switch token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new KeywordElement("switch ", token),
                    new OperatorElement("(", token),
                    token.Condition.ToFrameworkElement(),
                    new OperatorElement(")", token)
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this While token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new KeywordElement("while ", token),
                    new OperatorElement("(", token),
                    token.Condition.ToFrameworkElement(),
                    new OperatorElement(")", token)
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this Call token)
        {
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new MethodNameElement(token.Name, token),
                    new OperatorElement("(", token)
                },
                Tag = token
            };

            if (token.Arguments != null)
                for (var index = 0; index < token.Arguments.Count; index++)
                {
                    var arg = token.Function.Arguments[index];
                    var value = token.Arguments[index];

                    if (arg.Type == AtelType.Integer)
                    {
                        if (value is Value valueValue && valueValue.Type == ValueType.Int)
                        {
                            tokenPanel.Children.Add(arg.ShowAsHex
                                ? new NumberElement($"0x{valueValue.IntValue:X8}", valueValue)
                                : new NumberElement(valueValue.IntValue.ToString(), valueValue));
                        }
                        else if (value is GetConstantInt valueConstant)
                        {
                            tokenPanel.Children.Add(arg.ShowAsHex
                                ? new NumberElement($"0x{valueConstant.Constant.Value:X8}", valueConstant)
                                : new NumberElement(valueConstant.Constant.Value.ToString(), valueConstant));
                        }
                        else
                        {
                            tokenPanel.Children.Add(value.ToFrameworkElement());
                        }
                    }
                    else
                    {
                        tokenPanel.Children.Add(value.ToFrameworkElement());
                    }

                    if (index != token.Arguments.Count - 1)
                        tokenPanel.Children.Add(new OperatorElement(", ", token));
                }

            tokenPanel.Children.Add(new OperatorElement(")", token));

            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this DeclareVariable token)
        {
            var varType = "unknown";

            switch (token.Variable.Type)
            {
                case AtelLib.ATEL.Disassembler.Enums.VariableType.Bit:
                    varType = "bit";
                    break;
                case AtelLib.ATEL.Disassembler.Enums.VariableType.Char:
                    varType = "char";
                    break;
                case AtelLib.ATEL.Disassembler.Enums.VariableType.Int:
                    varType = "int";
                    break;
                case AtelLib.ATEL.Disassembler.Enums.VariableType.Float:
                    varType = "bit";
                    break;
            }

            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new KeywordElement(varType + " ", token),
                    new VariableElement(token.Variable, token)
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this GenericToken token)
        {
            return new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0)),
                Text = token.Name,
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this GetConstantInt token)
        {
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new NumberElement(token.Constant.Value.ToString(), token)
                },
                Tag = token
            };
            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this GetConstantFloat token)
        {
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new NumberElement(token.Constant.Value.ToString(), token)
                },
                Tag = token
            };
            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this GetRegister token)
        {
            return new RegisterElement(token.Register, token);
        }

        public static FrameworkElement ToFrameworkElement(this GetVariable token)
        {
            if (token.Index != null)
            {
                return new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Children =
                    {
                        new VariableElement(token.Variable, token),
                        new TextBlock
                        {
                            Text = "["
                        },
                        token.Index.ToFrameworkElement(),
                        new TextBlock
                        {
                            Text = "]"
                        },
                    },
                    Tag = token
                };
            }
            else
            {
                return new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Children =
                    {
                        new VariableElement(token.Variable, token)
                    },
                    Tag = token
                };
            }
        }

        public static FrameworkElement ToFrameworkElement(this Goto token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new TextBlock
                    {
                        Text = "goto ",
                        Foreground = new SolidColorBrush(SyntaxColors.Keyword)
                    },
                    new TextBlock
                    {
                        Text = token.Destination.Name,
                        Foreground = new SolidColorBrush(SyntaxColors.Operator)
                    }
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this IntValue token)
        {
            return new TextBlock
            {
                Text = token.Value.ToString(),
                Foreground = SyntaxBrushes.Number,
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this FloatValue token)
        {
            return new TextBlock
            {
                Text = token.Value.ToString(),
                Foreground = SyntaxBrushes.Number,
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this StringValue token)
        {
            return new TextBlock
            {
                Text = token.Value,
                Foreground = SyntaxBrushes.Number,
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this Label token)
        {
            return new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new TextBlock
                    {
                        Text = token.SourceLabel.Name,
                        Foreground = SyntaxBrushes.Operator
                    },
                    new TextBlock
                    {
                        Text = ":"
                    }
                },
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this Operation token)
        {
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                },
                Tag = token
            };

            switch (token.Type)
            {
                case OperatorType.NOT:
                    tokenPanel.Children.Add(new TextBlock
                    {
                        Text = $"{token.GetOperatorString()}",
                        Foreground = SyntaxBrushes.Operator
                    });
                    tokenPanel.Children.Add(token.LeftOperand.ToFrameworkElement());
                    break;
                default:
                    tokenPanel.Children.Add(token.LeftOperand.ToFrameworkElement());
                    tokenPanel.Children.Add(new TextBlock
                    {
                        Text = $" {token.GetOperatorString()} ",
                        Foreground = SyntaxBrushes.Operator
                    });
                    if (token.RightOperand != null)
                        tokenPanel.Children.Add(token.RightOperand.ToFrameworkElement());
                    break;
            }

            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this Return token)
        {
            return new TextBlock
            {
                Text = token.ToString(),
                Foreground = SyntaxBrushes.Keyword,
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this SetRegister token)
        {
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new RegisterElement(token.Register, token)
                },
                Tag = token
            };

            // register++
            if (token.Value is Operation incOperation &&
                incOperation.Type == OperatorType.ADD &&
                incOperation.LeftOperand is GetRegister incVariable &&
                incVariable.Register == token.Register &&
                incOperation.RightOperand is Value incValue &&
                incValue.Type == ValueType.Int &&
                incValue.IntValue == 1)
            {
                tokenPanel.Children.Add(new TextBlock {Text = "++", Foreground = SyntaxBrushes.Operator});
                return tokenPanel;
            }

            // register += value
            if (token.Value is Operation addOperation &&
                addOperation.Type == OperatorType.ADD &&
                addOperation.LeftOperand is GetRegister addVariable &&
                addVariable.Register == token.Register &&
                addOperation.RightOperand is Value addValue &&
                addValue.Type == ValueType.Int &&
                addValue.IntValue == 1)
            {
                tokenPanel.Children.Add(new TextBlock {Text = " += ", Foreground = SyntaxBrushes.Operator});
                tokenPanel.Children.Add(token.Value.ToFrameworkElement());
                return tokenPanel;
            }

            // register--
            if (token.Value is Operation decOperation &&
                decOperation.Type == OperatorType.SUB &&
                decOperation.LeftOperand is GetRegister decVariable &&
                decVariable.Register == token.Register &&
                decOperation.RightOperand is Value decValue &&
                (decValue.Type == ValueType.Int ||
                 decValue.Type == ValueType.Float))
            {
                tokenPanel.Children.Add(new TextBlock {Text = "--", Foreground = SyntaxBrushes.Operator});
                return tokenPanel;
            }

            // register -= value
            if (token.Value is Operation subOperation &&
                subOperation.Type == OperatorType.SUB &&
                subOperation.LeftOperand is GetRegister subVariable &&
                subVariable.Register == token.Register &&
                subOperation.RightOperand is Value subValue &&
                (subValue.Type == ValueType.Int ||
                 subValue.Type == ValueType.Float))
            {
                tokenPanel.Children.Add(new TextBlock {Text = " -= ", Foreground = SyntaxBrushes.Operator});
                tokenPanel.Children.Add(token.Value.ToFrameworkElement());
                return tokenPanel;
            }

            // Standard expression
            tokenPanel.Children.Add(new TextBlock
            {
                Text = " = ",
                Foreground = SyntaxBrushes.Operator
            });
            tokenPanel.Children.Add(token.Value.ToFrameworkElement());

            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this SetVariable token)
        {
            // Create panel with variable
            var tokenPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Children =
                {
                    new VariableElement(token.Variable, token)
                },
                Tag = token
            };

            // Add index if applicable
            if (token.Variable.Length > 1 || token.Index != null)
            {
                tokenPanel.Children.Add(new TextBlock {Text = "["});
                tokenPanel.Children.Add(token.Index.ToFrameworkElement());
                tokenPanel.Children.Add(new TextBlock {Text = "]"});
            }

            // variable++
            if (token.Value is Operation incOperation &&
                incOperation.Type == OperatorType.ADD &&
                incOperation.LeftOperand is GetVariable incVariable &&
                incVariable.Variable == token.Variable &&
                incOperation.RightOperand is Value incValue &&
                incValue.Type == ValueType.Int &&
                incValue.IntValue == 1)
            {
                tokenPanel.Children.Add(new TextBlock {Text = "++", Foreground = SyntaxBrushes.Operator});
                return tokenPanel;
            }

            // variable += value
            if (token.Value is Operation addOperation &&
                addOperation.Type == OperatorType.ADD &&
                addOperation.LeftOperand is GetVariable addVariable &&
                addVariable.Variable == token.Variable &&
                addOperation.RightOperand is Value addValue &&
                addValue.Type == ValueType.Int &&
                addValue.IntValue == 1)
            {
                tokenPanel.Children.Add(new TextBlock {Text = " += ", Foreground = SyntaxBrushes.Operator});
                tokenPanel.Children.Add(token.Value.ToFrameworkElement());
                return tokenPanel;
            }

            // variable--
            if (token.Value is Operation decOperation &&
                decOperation.Type == OperatorType.SUB &&
                decOperation.LeftOperand is GetVariable decVariable &&
                decVariable.Variable == token.Variable &&
                decOperation.RightOperand is Value decValue &&
                (decValue.Type == ValueType.Int ||
                 decValue.Type == ValueType.Float))
            {
                tokenPanel.Children.Add(new TextBlock {Text = "--", Foreground = SyntaxBrushes.Operator});
                return tokenPanel;
            }

            // variable -= value
            if (token.Value is Operation subOperation &&
                subOperation.Type == OperatorType.SUB &&
                subOperation.LeftOperand is GetVariable subVariable &&
                subVariable.Variable == token.Variable &&
                subOperation.RightOperand is Value subValue &&
                (subValue.Type == ValueType.Int ||
                 subValue.Type == ValueType.Float))
            {
                tokenPanel.Children.Add(new TextBlock {Text = " -= ", Foreground = SyntaxBrushes.Operator});
                tokenPanel.Children.Add(token.Value.ToFrameworkElement());
                return tokenPanel;
            }

            // Standard expression
            tokenPanel.Children.Add(new TextBlock
            {
                Text = " = ",
                Foreground = SyntaxBrushes.Operator
            });
            tokenPanel.Children.Add(token.Value.ToFrameworkElement());

            return tokenPanel;
        }

        public static FrameworkElement ToFrameworkElement(this Value token)
        {
            switch (token.Type)
            {
                case ValueType.String:
                    return new TextBlock
                    {
                        Text = token.StringValue,
                        Tag = token
                    };
                case ValueType.Int:
                    return new TextBlock
                    {
                        Text = token.IntValue.ToString(),
                        Foreground = SyntaxBrushes.Number,
                        Tag = token
                    };
                case ValueType.Float:
                    return new TextBlock
                    {
                        Text = token.FloatValue.ToString(),
                        Foreground = SyntaxBrushes.Number,
                        Tag = token
                    };
            }

            return new TextBlock
            {
                Text = "???",
                Tag = token
            };
        }

        public static FrameworkElement ToFrameworkElement(this VariableConstant token)
        {
            return new VariableElement(token, null);
        }
    }
}