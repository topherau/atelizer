﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Disassembler.Classes.Constants;

namespace Atelizer.Interface.Elements
{
    public class VariableElement : TextBlock
    {
        public VariableElement(VariableConstant variable, IToken token)
        {
            Tag = token;
            Foreground = SyntaxBrushes.Variable;

            base.SetBinding(TextProperty, new Binding {Source = variable, Path = new PropertyPath("Name")});
        }
    }
}