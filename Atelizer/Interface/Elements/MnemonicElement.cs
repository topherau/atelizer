﻿using System.Windows.Controls;
using AtelLib.ATEL.Disassembler.Classes;

namespace Atelizer.Interface.Elements
{
    public class MnemonicElement : TextBlock
    {
        public MnemonicElement(Opcode opcode)
        {
            Text = opcode.Mnemonic + " ";
            Foreground = SyntaxBrushes.Mnemonic;
        }
    }
}