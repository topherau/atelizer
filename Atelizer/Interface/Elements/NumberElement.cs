﻿using System.Windows.Controls;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Elements
{
    public class NumberElement : TextBlock
    {
        public NumberElement(string text, IToken token)
        {
            Tag = token;
            Text = text;
            Foreground = SyntaxBrushes.Number;
        }
    }
}