﻿using System.Windows.Controls;
using AtelLib.ATEL.Decompiler.Enums;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Elements
{
    public class RegisterElement : TextBlock
    {
        public RegisterElement(RegisterType register, IToken token)
        {
            Text = register.ToString();
            Foreground = SyntaxBrushes.Register;
            Tag = token;
        }
    }
}