﻿using System.Windows.Controls;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Elements
{
    public class OperatorElement : TextBlock
    {
        public OperatorElement(string text, IToken token)
        {
            Tag = token;
            Text = text;
            Foreground = SyntaxBrushes.Operator;
        }
    }
}