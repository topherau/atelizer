﻿using System.Windows.Controls;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Elements
{
    public class MethodNameElement : TextBlock
    {
        public MethodNameElement(string text, IToken token)
        {
            Tag = token;
            Text = text;
            Foreground = SyntaxBrushes.Method;
        }
    }
}