﻿using System.Windows.Controls;
using AtelLib.ATEL.Decompiler.Interfaces;

namespace Atelizer.Interface.Elements
{
    public class KeywordElement : TextBlock
    {
        public KeywordElement(string text, IToken token)
        {
            Tag = token;
            Text = text;
            Foreground = SyntaxBrushes.Keyword;
        }
    }
}