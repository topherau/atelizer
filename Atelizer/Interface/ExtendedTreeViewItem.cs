﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace Atelizer.Interface
{
    public class ExtendedTreeViewItem : TreeViewItem
    {
        private Grid _grid;

        public ExtendedTreeViewItem()
        {
            _grid = this.FindChild<Grid>("ItemGrid");
        }

        public Thickness MarginDepth
        {
            get
            {
                int depth=0;
                TreeViewItem tvi = this;
                while (true)
                {
                    if (tvi.Parent is TreeViewItem parentTvi)
                    {
                        depth++;
                        tvi = parentTvi;
                        continue;
                    }

                    break;
                }

                var marginValue = (depth) * 28 + 35;
                return new Thickness(-marginValue, 0, -marginValue, 0);
            }
        }
    }
}