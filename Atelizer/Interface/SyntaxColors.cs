﻿using System.Windows.Media;

namespace Atelizer.Interface
{
    public static class SyntaxColors
    {
        public static Color Keyword { get; }= Color.FromRgb(86, 156, 214);
        public static Color Method { get; } = Color.FromRgb(0, 255, 255);
        public static Color Operator { get; } = Color.FromRgb(180, 180, 180);
        public static Color Number { get; } = Color.FromRgb(181, 206, 168);
        public static Color String { get; } = Color.FromRgb(214, 157, 133);
        public static Color Variable { get; } = Color.FromRgb(238, 130, 238);
        public static Color Register { get; } = Color.FromRgb(78, 201, 176);
        public static Color Mnemonic { get; } = Color.FromRgb(146, 202, 244);
        public static Color LineNumber { get; } = Color.FromRgb(43, 145, 175);
    }

    public static class SyntaxBrushes
    {
        public static SolidColorBrush Keyword { get; } = new SolidColorBrush(SyntaxColors.Keyword);
        public static SolidColorBrush Method { get; } = new SolidColorBrush(SyntaxColors.Method);
        public static SolidColorBrush Operator { get; } = new SolidColorBrush(SyntaxColors.Operator);
        public static SolidColorBrush Number { get; } = new SolidColorBrush(SyntaxColors.Number);
        public static SolidColorBrush String { get; } = new SolidColorBrush(SyntaxColors.String);
        public static SolidColorBrush Variable { get; } = new SolidColorBrush(SyntaxColors.Variable);
        public static SolidColorBrush Register { get; } = new SolidColorBrush(SyntaxColors.Register);
        public static SolidColorBrush Mnemonic { get; } = new SolidColorBrush(SyntaxColors.Mnemonic);
        public static SolidColorBrush LineNumber { get; } = new SolidColorBrush(SyntaxColors.LineNumber);
    }
}