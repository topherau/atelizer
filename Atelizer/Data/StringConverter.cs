﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FFXLib.Extension;

namespace FFXLib.FileSystem
{
    public class StringConverter
    {
        public static Dictionary<int, ObservableCollection<string>> MacroDictionary;
        private List<char> _encodingTable;

        public StringConverter(IEnumerable<char> encodingTable)
        {
            _encodingTable = encodingTable.ToList();
        }

        #region Decoding

        public string Decode(byte[] input, int startOffset)
        {
            if (input == null)
                return null;
            var stream = new MemoryStream(input);
            stream.Seek(startOffset, SeekOrigin.Begin);

            var reader = new BinaryReader(stream);
            var output = new StringBuilder();

            while (stream.Position < stream.Length)
            {
                var inByte = reader.ReadByte();
                
                if (inByte < 0x30)
                {
                    // Handle special character code
                    var special = GetSpecialCharacter(inByte, stream);

                    if (special == null)
                        goto end;

                    output.Append(special);
                    continue;
                }
                
                output.Append(_encodingTable[inByte - 0x30]);
            }

            end:

            return output.ToString();
        }

        private string GetSpecialCharacter(byte code, Stream stream)
        {
            // Process codes without parameter first

            // String terminator
            if (code == 0x00)
                return null;

            // New line
            if (code == 0x03)
                return "\n";

            // Read code parameter
            var parameter = stream.ReadUByte();

            // Process codes with parameter
            switch (code)
            {
                case 0x0A: // Color change
                    return GetColorString(parameter);
                case 0x0B: // Button icon
                    return GetButtonString(parameter);
                case 0x0E: // Style
                    return GetStyleString(parameter);
                case 0x12: // Data parameter
                    return GetDataString(parameter);
                case 0x13: // Name
                    return GetNameString(parameter);

                // Character page modifiers
                case 0x2C:
                    return _encodingTable[0xA0 + parameter].ToString();
                case 0x2D:
                    return _encodingTable[0x170 + parameter].ToString();
                case 0x2E:
                    return _encodingTable[0x240 + parameter].ToString();
                case 0x2F:
                    return _encodingTable[0x310 + parameter].ToString();

                // Unknown/macro
                default:
                    return "{" + GetMacroCode(code, parameter) + "}";
            }
        }

        private static string GetMacroCode(byte macroCode, byte parameter)
        {
            var macroText = $"@{macroCode:X2}:{parameter:X2}";

            if (MacroDictionary == null)
                return macroText;

            switch (macroCode)
            {
                case 0x19:
                    macroText += "#" + MacroDictionary[6][parameter - 0x30];
                    break;
                case 0x20:
                    macroText += "#" + MacroDictionary[13][parameter - 0x30];
                    break;
                default:
                    return macroText;
            }

            return macroText;
        }

        private static string GetColorString(byte colorCode)
        {
            return $"{{COLOR:{colorCode.ToString("x")}}}";
        }

        private static string GetButtonString(byte buttonCode)
        {
            return $"{{BUTTON:{buttonCode.ToString("x")}}}";
        }

        private static string GetDataString(byte readByte)
        {
            return $"{{DATA:{readByte.ToString("x")}}}";
        }

        private static string GetStyleString(byte styleCode)
        {
            // Known styles
            //  0x40 Italic
            //  0x41 Normal
            return $"{{STYLE:{styleCode.ToString("x")}}}";
        }

        private static string GetNameString(byte nameCode)
        {
            switch (nameCode)
            {
                case 0x30:
                    return "{TIDUS}";
                case 0x31:
                    return "{YUNA}";
                case 0x32:
                    return "{AURON}";
                case 0x33:
                    return "{KIMAHRI}";
                case 0x34:
                    return "{WAKKA}";
                case 0x35:
                    return "{LULU}";
                case 0x36:
                    return "{RIKKU}";
                case 0x37:
                    return "{SEYMOUR}";
                case 0x38:
                    return "{VALEFOR}";
                case 0x39:
                    return "{IFRIT}";
                case 0x3a:
                    return "{IXION}";
                case 0x3b:
                    return "{SHIVA}";
                case 0x3c:
                    return "{BAHAMUT}";
                case 0x3d:
                    return "{ANIMA}";
                case 0x3e:
                    return "{YOJIMBO}";
                case 0x3f:
                    return "{CINDY}";
                case 0x40:
                    return "{SANDY}";
                case 0x41:
                    return "{MINDY}";
            }

            return $"{{NAME:{nameCode.ToString("X2")}}}";
        }

        #endregion

        #region Encoding

        public byte[] Encode(string input)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(input));
            var reader = new BinaryReader(stream);
            var writer = new BinaryWriter(new MemoryStream());

            while (stream.Position < stream.Length)
            {
                var inChar = reader.ReadChar();
                if (inChar == '{')
                {
                    var encodedBytes = EncodeSpecialCharacter(reader);
                    if (encodedBytes == null)
                        throw new Exception($"Unable to encode string \"{input}\" - invalid special input code");
                    writer.Write(encodedBytes);
                }
                else if (_encodingTable.Contains(inChar))
                {
                    var charIndex = _encodingTable.IndexOf(inChar);

                    if (charIndex >= 0xA0)
                    {
                        // Kanji table
                        var page = (charIndex - 0xa0) / 0xd0;
                        var pgIndex = (charIndex - 0xa0) % 0xd0;

                        writer.Write((byte) (page + 0x2C));
                        writer.Write((byte) pgIndex);
                    }
                    else
                    {
                        // Standard character
                        writer.Write((byte) (charIndex + 0x30));
                    }
                }
                else
                {
                    throw new Exception(
                        $"Unable to encode character \"{inChar}\": The character was not found in the encoding table.");
                }
            }

            writer.Write('\0');

            return ((MemoryStream) writer.BaseStream).ToArray();
        }

        private static byte[] EncodeSpecialCharacter(BinaryReader reader)
        {
            var specialBuilder = new StringBuilder();
            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                // Read next byte from stream
                var nextByte = reader.ReadByte();
                if (nextByte == '}' || //	End of special input
                    nextByte == '\0') //	End of string
                {
                    break;
                }

                specialBuilder.Append((char) nextByte);
            }

            var special = specialBuilder.ToString().Trim();

            if (special.StartsWith("@"))
            {
                // Encode hex characters
                if (special.Contains("#"))
                    special = special.Substring(0, special.IndexOf("#", StringComparison.Ordinal));

                return EncodeHexString(special);
            }
            else
            {
                if (special.Contains(":"))
                {
                    return EncodeSpecialCode(special);
                }
                else
                {
                    return EncodeSpecialString(special);
                }
            }
        }

        private static Dictionary<string, byte[]> SpecialStrings = new Dictionary<string, byte[]>
        {
            ["TIDUS"] = new byte[] {0x13, 0x30},
            ["YUNA"] = new byte[] { 0x13, 0x31 },
            ["AURON"] = new byte[] { 0x13, 0x32 },
            ["KIMAHRI"] = new byte[] { 0x13, 0x33 },
            ["WAKKA"] = new byte[] { 0x13, 0x34 },
            ["LULU"] = new byte[] { 0x13, 0x35 },
            ["RIKKU"] = new byte[] { 0x13, 0x36 },
            ["SEYMOUR"] = new byte[] { 0x13, 0x37 },
            ["VALEFOR"] = new byte[] { 0x13, 0x38 },
            ["IFRIT"] = new byte[] { 0x13, 0x39 },
            ["IXION"] = new byte[] { 0x13, 0x3a },
            ["SHIVA"] = new byte[] { 0x13, 0x3b },
            ["BAHAMUT"] = new byte[] { 0x13, 0x3c },
            ["ANIMA"] = new byte[] { 0x13, 0x3d },
            ["YOJIMBO"] = new byte[] { 0x13, 0x3e },
            ["CINDY"] = new byte[] { 0x13, 0x3f },
            ["SANDY"] = new byte[] { 0x13, 0x40 },
            ["MINDY"] = new byte[] { 0x13, 0x41 },

            //["TRIANGLE"] = new byte[] { 0x0B, 0x30 },
            //["CIRCLE"] = new byte[] { 0x0B, 0x31 },
            //["CROSS"] = new byte[] { 0x0B, 0x32 },
            //["SQUARE"] = new byte[] { 0x0B, 0x33 },
            //["L1"] = new byte[] { 0x0B, 0x34 },
            //["R1"] = new byte[] { 0x0B, 0x35 },
            //["L2"] = new byte[] { 0x0B, 0x36 },
            //["R2"] = new byte[] { 0x0B, 0x37 },
            //["START"] = new byte[] { 0x0B, 0x38 },
            //["SELECT"] = new byte[] { 0x0B, 0x39 },

        };

        private static Dictionary<string, byte> SpecialCodes = new Dictionary<string, byte>
        {
            ["COLOR"] = 0x0A,
            ["BUTTON"] = 0x0B,
            ["STYLE"] = 0x0E,
            ["DATA"] = 0x12,
            ["NAME"] = 0x13,
        };

        private static byte[] EncodeHexString(string hexString)
        {
            // Encode a string of hex characters in XX:XX:XX:XX format
            var characters = hexString.Split(':');
            var bytes = new byte[characters.Length];

            for (var i = 0; i < characters.Length; i++)
            {
                var parsed = byte.TryParse(characters[i], NumberStyles.HexNumber, CultureInfo.CurrentCulture,
                    out var parsedByte);
                if (!parsed)
                    throw new Exception("Invalid hex character!");
                bytes[i] = parsedByte;
            }

            return bytes;
        }

        private static byte[] EncodeSpecialCode(string specialCode)
        {
            // Encode a special code, i.e. COLOR:XX or STYLE:XX
            var code = specialCode.Split(new []{':'}, 2);
            if (code.Length != 2)
                throw new Exception("Invalid special code.");

            var special = SpecialCodes.FirstOrDefault(s => string.Compare(s.Key, code[0], StringComparison.OrdinalIgnoreCase) != 0);
            if (special.Key == null)
                throw new Exception("Unknown special code.");

            var bytes = EncodeHexString(code[2]);
            var result = new byte[bytes.Length + 1];
            result[0] = special.Value;
            bytes.CopyTo(result, 1);
            return result;
        }

        private static byte[] EncodeSpecialString(string specialString)
        {
            var special = SpecialStrings.FirstOrDefault(s => string.Compare(s.Key, specialString, StringComparison.OrdinalIgnoreCase) != 0);
            if (special.Key == null)
                throw new Exception("Invalid special string.");
            return special.Value;
        }

        #endregion
    }
}