﻿using System;
using System.IO;

namespace FFXLib.Extension
{
    public static class StreamExtension
    {
        public static ulong ReadUInt64(this Stream stream)
        {
            return BitConverter.ToUInt64(stream.ReadBytes(8), 0);
        }

        public static uint ReadUInt32(this Stream stream)
        {
            return BitConverter.ToUInt32(stream.ReadBytes(4), 0);
        }

        public static ushort ReadUInt16(this Stream stream)
        {
            return BitConverter.ToUInt16(stream.ReadBytes(2), 0);
        }

        public static byte ReadUByte(this Stream stream)
        {
            return (byte) stream.ReadByte();
        }

        public static long ReadInt64(this Stream stream)
        {
            return BitConverter.ToInt64(stream.ReadBytes(8), 0);
        }

        public static int ReadInt32(this Stream stream)
        {
            return BitConverter.ToInt32(stream.ReadBytes(4), 0);
        }

        public static short ReadInt16(this Stream stream)
        {
            return BitConverter.ToInt16(stream.ReadBytes(2), 0);
        }

        public static sbyte ReadSByte(this Stream stream)
        {
            return (sbyte) stream.ReadByte();
        }

        public static float ReadSingle(this Stream stream)
        {
            return BitConverter.ToSingle(stream.ReadBytes(4), 0);
        }

        public static byte[] ReadBytes(this Stream stream, int count)
        {
            var buffer = new byte[count];
            stream.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        public static void Write(this Stream stream, ulong value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }

        public static void Write(this Stream stream, uint value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }

        public static void Write(this Stream stream, ushort value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 2);
        }

        public static void Write(this Stream stream, byte value)
        {
            stream.WriteByte(value);
        }

        public static void Write(this Stream stream, long value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }

        public static void Write(this Stream stream, int value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }

        public static void Write(this Stream stream, short value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 2);
        }

        public static void Write(this Stream stream, sbyte value)
        {
            stream.WriteByte((byte)value);
        }

        public static void WriteBytes(this Stream stream, byte[] bytes)
        {
            stream.Write(bytes, 0, bytes.Length);
        }
    }
}