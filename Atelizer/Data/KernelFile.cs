﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using FFXLib.Extension;
using FFXLib.FileSystem.Data.Records;

namespace FFXLib.FileSystem.Data
{
    public class KernelFile<TRecord> where TRecord : Record, new()
    {
        public TRecord this[int recordIndex]
        {
            get => Records[recordIndex];
            set => Records[recordIndex] = value;
        }

        public ObservableCollection<TRecord> Records { get; set; }
        public Type RecordType => typeof(TRecord);

        public delegate void RecordPropertyChangedHandler(int recordIndex, string propertyName);

        public event RecordPropertyChangedHandler RecordPropertyChanged;

        private readonly Stream _stream;
        private readonly int _pageId;

        private ushort _dataLength;
        private ushort _lastRecordIndex;
        private ushort _recordLength;
        private byte[] _stringTable;
        private uint _dataStart;

        public KernelFile(Stream stream, int pageId = 0)
        {
            _stream = stream;
            _pageId = pageId;

            Load();
        }

        public bool Contains(TRecord record) => Records.Contains(record);
        public int IndexOf(TRecord record) => Records.IndexOf(record);

        private void Load()
        {
            // Return to beginning of stream
            _stream.Seek(0, SeekOrigin.Begin);

            // Read header
            var header = ReadUInt32();
            if (header != 1)
                throw new Exception("Invalid kernel file (wrong header)");

            _stream.Seek(6, SeekOrigin.Current);
            _lastRecordIndex = ReadUInt16();
            _recordLength = ReadUInt16();
            _dataLength = ReadUInt16();
            _dataStart = ReadUInt32();

            var recordCount = _dataLength / _recordLength;

            // Read strings from file
            var stringTablePos = _dataLength + _dataStart;
            var stringTableLength = (int) (_stream.Length - stringTablePos);
            if (stringTableLength > 0)
            {
                _stream.Seek(stringTablePos, SeekOrigin.Begin);
                _stringTable = ReadBytes(stringTableLength);

                // Check that the correct number of strings are defined in the record
                var numStringsInRecord = typeof(TRecord).GetProperties().Count(p =>
                    p.PropertyType == typeof(string) && p.GetCustomAttribute<RecordDataAttribute>() != null);
                var numStringsInFile = _stringTable.Count(c => c == '\0');
                if (numStringsInFile / recordCount != numStringsInRecord)
                    throw new Exception(
                        $"Number of strings in file ({numStringsInFile}) does not match number of strings in record ({numStringsInRecord}).");
            }
            else
            {
                _stringTable = new byte[0];
            }

            // Read records
            var records = new List<TRecord>();
            for (var i = 0; i < recordCount; i++)
            {
                // Read record
                var record = ReadRecord(i);
                record.PropertyChanged += Record_OnPropertyChanged;
                records.Add(record);
            }

            Records = new ObservableCollection<TRecord>(records);
        }

        public byte[] GetBytes()
        {
            var recordStream = new MemoryStream();
            var stringStream = new MemoryStream();

            // write header
            recordStream.Write(1);

            recordStream.Seek(0xA, SeekOrigin.Begin);
            recordStream.Write((ushort) (Records.Count - 1));
            recordStream.Write(_recordLength);
            recordStream.Write((ushort) (Records.Count * _recordLength));
            recordStream.Write((uint)0x14);

            foreach (var record in Records)
            {
                // Retrieve a list of string properties to encode
                var strings = typeof(TRecord).GetProperties().Where(p =>
                    p.PropertyType == typeof(string) && p.GetCustomAttribute<RecordDataAttribute>() != null);

                foreach (var stringProperty in strings)
                {
                    // Get value of string and encode
                    var value = (string)stringProperty.GetValue(record);
                    var bytes = Record.StringConverter.Encode(value);

                    // Set string offset, write encoded string to stream
                    record.SetStringOffset(stringProperty, (uint)stringStream.Position);
                    stringStream.WriteBytes(bytes);
                }

                // Write record bytes to stream
                recordStream.WriteBytes(record.GetBytes());
            }

            stringStream.WriteTo(recordStream);
            return recordStream.ToArray();
        }

        public void Record_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RecordPropertyChanged?.Invoke(Records.IndexOf((TRecord) sender), e.PropertyName);
        }

        private TRecord ReadRecord(int recordIndex)
        {
            _stream.Seek(_dataStart + recordIndex * _recordLength, SeekOrigin.Begin);
            var bytes = ReadBytes(_recordLength);
            var record = Record.Parse<TRecord>(bytes, _stringTable);
            return record;
        }

        private uint ReadUInt32()
        {
            var buffer = new byte[4];
            _stream.Read(buffer, 0, 4);
            return BitConverter.ToUInt32(buffer, 0);
        }

        private ushort ReadUInt16()
        {
            var buffer = new byte[2];
            _stream.Read(buffer, 0, 2);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private byte[] ReadBytes(int length)
        {
            var buffer = new byte[length];
            _stream.Read(buffer, 0, length);
            return buffer;
        }
    }
}