﻿using System.Collections.ObjectModel;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class Monster : Record
    {
        [RecordData(0x0000)] public string Name { get; set; }

        [RecordData(0x0004)] public string Description { get; set; }

        [RecordData(0x0008)] public string String1 { get; set; }

        [RecordData(0x000c)] public string ScanMessage { get; set; }

        [RecordData(0x0010)] public string String2 { get; set; }

        [RecordData(0x0014)] public uint MaxHp { get; set; }

        [RecordData(0x0018)] public uint MaxMp { get; set; }

        [RecordData(0x001C)] public uint Overkill { get; set; }

        [RecordData(0x0020)] public byte Strength { get; set; }

        [RecordData(0x0021)] public byte Defense { get; set; }

        [RecordData(0x0022)] public byte Magic { get; set; }

        [RecordData(0x0023)] public byte MagicDefense { get; set; }

        [RecordData(0x0024)] public byte Agility { get; set; }

        [RecordData(0x0025)] public byte Luck { get; set; }

        [RecordData(0x0026)] public byte Evasion { get; set; }

        [RecordData(0x0027)] public byte Accuracy { get; set; }

        [RecordData(0x0028)] public ushort Unknown0028 { get; set; }

        //[RecordData(0x0029)] public byte Unknown0029 { get; set; }

        [RecordData(0x002A)] public byte PoisonDamagePercent { get; set; }

        [RecordData(0x002b)] public byte ElementalAbsorb { get; set; }

        [RecordData(0x002c)] public byte ElementalImmune { get; set; }

        [RecordData(0x002d)] public byte ElementalResist { get; set; }

        [RecordData(0x002e)] public byte ElementalVulnerable { get; set; }

        [RecordData(0x002f)] public byte KoResist { get; set; }

        [RecordData(0x0030)] public byte ZombieResist { get; set; }

        [RecordData(0x0031)] public byte PetrifyResist { get; set; }

        [RecordData(0x0032)] public byte PoisonResist { get; set; }
        
        [RecordData(0x0033)] public byte PowerBreakResist { get; set; }
        
        [RecordData(0x0034)] public byte MagicBreakResist { get; set; }
        
        [RecordData(0x0035)] public byte ArmorBreakResist { get; set; }
        
        [RecordData(0x0036)] public byte MentalBreakResist { get; set; }
        
        [RecordData(0x0037)] public byte ConfusionResist { get; set; }
        
        [RecordData(0x0038)] public byte BerserkResist { get; set; }
        
        [RecordData(0x0039)] public byte ProvokeResist { get; set; }
        
        [RecordData(0x003a)] public byte ThreatenResist { get; set; }

        [RecordData(0x003b)] public byte SleepResist { get; set; }

        [RecordData(0x003c)] public byte SilenceResist { get; set; }

        [RecordData(0x003d)] public byte DarknessResist { get; set; }

        [RecordData(0x003e)] public byte ShellResist { get; set; }

        [RecordData(0x003f)] public byte ProtectResist { get; set; }

        [RecordData(0x0040)] public byte ReflectResist { get; set; }

        [RecordData(0x0041)] public byte NulTideResist { get; set; }

        [RecordData(0x0042)] public byte NulBlazeResist { get; set; }

        [RecordData(0x0043)] public byte NulFrostResist { get; set; }

        [RecordData(0x0044)] public byte NulShockResist { get; set; }

        [RecordData(0x0045)] public byte RegenResist { get; set; }

        [RecordData(0x0046)] public byte HasteResist { get; set; }

        [RecordData(0x0047)] public byte SlowResist { get; set; }

        [RecordData(0x0048)] public ushort Unknown0048 { get; set; }

        //[RecordData(0x0049)] public byte Unknown0049 { get; set; }

        [RecordData(0x0050, 16)] public ObservableCollection<ushort> AbilityList { get; set; }

        [RecordData(0x0070)] public ushort BasicAttack { get; set; }

        [RecordData(0x0072)] public ushort MonsterId { get; set; }

        [RecordData(0x0077)] public byte DoomCount { get; set; }

        [RecordData(0x0078)] public byte Unknown0078 { get; set; }
        
        [RecordData(0x0079)] public byte Unknown0079 { get; set; }
        
        [RecordData(0x007a)] public byte Unknown007A { get; set; }
        
        [RecordData(0x007b)] public byte Unknown007B { get; set; }
        
        [RecordData(0x007c)] public byte Unknown007C { get; set; }
        
        [RecordData(0x007d)] public byte Unknown007D { get; set; }
        
        [RecordData(0x007e)] public byte Unknown007E { get; set; }
        
        [RecordData(0x007f)] public byte Unknown007F { get; set; }
    }
}