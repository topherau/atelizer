﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using FFXLib.Extension;
using Newtonsoft.Json;
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedMember.Local
// ReSharper disable VirtualMemberNeverOverridden.Global

namespace FFXLib.FileSystem.Data.Records
{
    public class Record : INotifyPropertyChanged
    {
        public static StringConverter StringConverter { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [JsonIgnore] public bool IsDirty { get; set; }

        private bool _disablePropertyChanged;
        private readonly int _recordLength = -1;
        private readonly Dictionary<PropertyInfo, uint> _stringOffsets = new Dictionary<PropertyInfo, uint>();

        protected Record()
        {
            var attribute = GetType().GetCustomAttribute<RecordLengthAttribute>();
            if (attribute != null)
                _recordLength = attribute.RecordLength;
        }

        public byte[] GetBytes()
        {
            var stream = new MemoryStream();
            if (_recordLength >= 0)
                stream.SetLength(_recordLength);

            var properties = GetType().GetProperties()
                .ToDictionary(p => p, p => p.GetCustomAttribute<RecordDataAttribute>());
            foreach (var property in properties)
            {
                if (property.Value == null)
                    continue;

                var attribute = property.Key.GetCustomAttribute<RecordDataAttribute>();
                if (attribute == null)
                    continue;

                stream.Seek(attribute.Offset, SeekOrigin.Begin);
                WriteProperty(property.Key, stream);
            }

            return stream.ToArray();
        }

        public static TRecord Parse<TRecord>(byte[] bytes, byte[] stringBytes = null) where TRecord : Record, new()
        {
            var stream = new MemoryStream(bytes);
            var record = new TRecord
            {
                _disablePropertyChanged = true
            };

            var properties = typeof(TRecord).GetProperties()
                .ToDictionary(p => p, p => p.GetCustomAttribute<RecordDataAttribute>());
            foreach (var property in properties)
            {
                if (property.Value == null)
                    continue;

                var value = ReadProperty(property.Key, property.Value, stream, stringBytes);
                property.Key.SetValue(record, value);
            }

            record._disablePropertyChanged = false;
            return record;
        }

        private static object ReadProperty(PropertyInfo property, RecordDataAttribute attribute, MemoryStream input, byte[] stringTable)
        {
            var propertyType = property.PropertyType;
            var propertyCount = 1;

            if (propertyType.IsArray)
            {
                // Array type
                propertyType = propertyType.GetElementType();
                if (propertyType == null)
                    throw new Exception($"Unable to get underlying type for array property \"{property.Name}\".");

                propertyCount = attribute.ArrayLength;
            }
            else if (propertyType.GetInterface("ICollection") != null)
            {
                // Generic collection type
                if (propertyType.GenericTypeArguments.Length != 1)
                    throw new Exception(
                        $"Unable to infer type of collection property \"{property.Name}\", a single generic type parameter is required.");

                propertyType = propertyType.GenericTypeArguments[0];
                propertyCount = attribute.ArrayLength;
            }

            if (propertyType.IsEnum)
            {
                propertyType = Enum.GetUnderlyingType(propertyType);
            }

            var propertyValues = Array.CreateInstance(propertyType, propertyCount);
            input.Seek(attribute.Offset, SeekOrigin.Begin);

            for (var i = 0; i < propertyCount; i++)
            {
                if (propertyType == typeof(ulong))
                {
                    propertyValues.SetValue(input.ReadUInt64(), i);
                }
                else if (propertyType == typeof(uint))
                {
                    propertyValues.SetValue(input.ReadUInt32(), i);
                }
                else if (propertyType == typeof(ushort))
                {
                    propertyValues.SetValue(input.ReadUInt16(), i);
                }
                else if (propertyType == typeof(byte))
                {
                    propertyValues.SetValue(input.ReadUByte(), i);
                }
                else if (propertyType == typeof(long))
                {
                    propertyValues.SetValue(input.ReadInt64(), i);
                }
                else if (propertyType == typeof(int))
                {
                    propertyValues.SetValue(input.ReadInt32(), i);
                }
                else if (propertyType == typeof(short))
                {
                    propertyValues.SetValue(input.ReadInt16(), i);
                }
                else if (propertyType == typeof(sbyte))
                {
                    propertyValues.SetValue(input.ReadSByte(), i);
                }
                else if (propertyType == typeof(string))
                {
                    var stringPos = input.ReadUInt16();
                    input.ReadUInt16(); // Advance 2 bytes for junk data
                    if (stringTable == null || stringPos >= stringTable.Length)
                        continue;

                    propertyValues.SetValue(StringConverter.Decode(stringTable, stringPos), i);
                }
            }

            if (propertyCount == 1)
            {
                return propertyValues.GetValue(0);
            }
            else
            {
                // Create the collection with the value array as parameter
                var collection = Activator.CreateInstance(property.PropertyType, propertyValues);
                return collection;
            }
        }

        private void WriteProperty(PropertyInfo property, MemoryStream output)
        {
            var attribute = property.GetCustomAttribute<RecordDataAttribute>();
            if (attribute == null)
                return;

            var propertyType = property.PropertyType;

            // Write the string offset if it is known
            if (propertyType == typeof(string))
            {
                output.Write(BitConverter.GetBytes(_stringOffsets.ContainsKey(property)
                        ? _stringOffsets[property] 
                        : 0), 0, 4);
                return;
            }

            var value = property.GetValue(this);
            var propertyCount = 1;

            Array propertyValues;
            if (propertyType.IsArray)
            {
                // Array type
                propertyType = propertyType.GetElementType();
                if (propertyType == null)
                    throw new Exception($"Unable to get underlying type for array property \"{property.Name}\".");

                propertyCount = attribute.ArrayLength;

                // Copy values to local array
                propertyValues = Array.CreateInstance(propertyType, propertyCount);
                Array.Copy((Array) value, propertyValues, propertyCount);
            }
            else if (propertyType.GetInterface("ICollection") != null)
            {
                // Generic collection type
                if (propertyType.GenericTypeArguments.Length != 1)
                    throw new Exception(
                        $"Unable to infer type of collection property \"{property.Name}\", a single generic type parameter is required.");

                propertyType = propertyType.GenericTypeArguments[0];
                propertyCount = attribute.ArrayLength;

                // Copy value collection into lical array
                propertyValues = Array.CreateInstance(propertyType, propertyCount);
                ((ICollection) value).CopyTo(propertyValues, 0);
            }
            else
            {
                // Not array type, copy single value
                propertyValues = Array.CreateInstance(propertyType, propertyCount);
                propertyValues.SetValue(value, 0);
            }

            // Get underlying type of enum
            if (propertyType.IsEnum)
                propertyType = Enum.GetUnderlyingType(propertyType);
            
            for (var i = 0; i < propertyCount; i++)
            {
                if (propertyType == typeof(ulong))
                    output.Write(BitConverter.GetBytes((ulong) value), 0, 8);
                else if (propertyType == typeof(uint))
                    output.Write(BitConverter.GetBytes((uint) value), 0, 4);
                else if (propertyType == typeof(ushort))
                    output.Write(BitConverter.GetBytes((ushort) value), 0, 2);
                else if (propertyType == typeof(byte))
                    output.Write(BitConverter.GetBytes((byte) value), 0, 1);
                else if (propertyType == typeof(long))
                    output.Write(BitConverter.GetBytes((long) value), 0, 8);
                else if (propertyType == typeof(int))
                    output.Write(BitConverter.GetBytes((int) value), 0, 4);
                else if (propertyType == typeof(short))
                    output.Write(BitConverter.GetBytes((short) value), 0, 2);
                else if (propertyType == typeof(sbyte))
                    output.Write(BitConverter.GetBytes((sbyte) value), 0, 1);
            }
        }

        public void SetStringOffset(PropertyInfo property, uint offset)
        {
            _stringOffsets[property] = offset;
        }

        private void OnCollectionChanged(PropertyInfo collectionInfo)
        {
            // Invoke PropertyChanged for collection modifications
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(collectionInfo.Name));
        }
        
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (_disablePropertyChanged || 
                propertyName == "IsDirty")
                return;

            IsDirty = true;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}