﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Atelizer.Interface;
using Atelizer.Interface.Elements;
using AtelLib.ATEL;
using AtelLib.ATEL.Database;
using AtelLib.ATEL.Decompiler;
using AtelLib.ATEL.Decompiler.Interfaces;
using AtelLib.ATEL.Decompiler.Tokens;
using AtelLib.ATEL.Decompiler.Tokens.Conditionals;
using AtelLib.ATEL.Disassembler;
using AtelLib.ATEL.Disassembler.Classes;
using AtelLib.ATEL.Disassembler.Classes.Constants;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using static System.Windows.Controls.TreeView;
using Label = AtelLib.ATEL.Decompiler.Tokens.Label;

namespace Atelizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private AtelReader _file;
        private AtelDisassembler _disassembler;
        private AtelDecompiler _decompiler;
        private Dictionary<uint, FrameworkElement> _asmList;
        private List<TreeViewItem> _decList;
        private Dictionary<uint, FrameworkElement> _tokens;
        private bool _isDecompilerLoaded;
        public bool IsDecompilerLoaded
        {
            get => _isDecompilerLoaded;
            set
            {
                _isDecompilerLoaded = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            AtelDatabase.Load("atelizer.db");
        }

        private void FileOpenBinary_OnClick(object sender, RoutedEventArgs e)
        {
            var openDialog = new OpenFileDialog
            {
                Title = "Open binary file",
                Filter = "Monster and event binaries|*.bin;*.ebp|All files|*.*"
            };

            if (!openDialog.ShowDialog() ?? true)
                return;

            LoadBinary(openDialog.FileName);
        }

        private void FileExitMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private async void LoadBinary(string filename)
        {
            var bytes = File.ReadAllBytes(filename);
            LoadBinary(bytes);
        }
        private async void LoadBinary(byte[] bytes)
        {
            IsDecompilerLoaded = false;
            OverlayPanel.Visibility = Visibility.Visible;

            _file = null;
            _disassembler = null;
            _decompiler?.Dispose();
            _decompiler = null;

            await Task.Run(async () =>
            {
                SetOverlayText("Loading...");
                _file = new AtelReader(bytes);

                try
                {
                    SetOverlayText("Disassembling...");
                    _disassembler = new AtelDisassembler(_file.GetScriptBytes());
                }
                catch (Exception ex)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        await this.ShowMessageAsync("Disassembler exception", ex.Message + "\n\n" + ex.StackTrace);
                    });
                    return;
                }

                try
                {
                    SetOverlayText("Decompiling...");
                    _decompiler = new AtelDecompiler(_disassembler);
                }
                catch (Exception ex)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        await this.ShowMessageAsync("Decompiler exception", ex.Message + "\n\n" + ex.StackTrace);
                    });
                    return;
                }
            });

            if (_disassembler != null)
            {
                BlockBox.Items.Clear();
                for (var i = 0; i < _disassembler.Blocks.Count; i++)
                    BlockBox.Items.Add($"Block {i}");
                BlockBox.SelectedIndex = 0;
                Title = $"{_disassembler.Filename} - Atelizer";
            }

            OverlayPanel.Visibility = Visibility.Collapsed;
            IsDecompilerLoaded = true;
        }

        private void SetOverlayText(string text)
        {
            OverlayText.Invoke(() => OverlayText.Text = text);
        }

        private void BlockBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BlockBox.SelectedIndex == -1)
                return;

            FunctionBox.Items.Clear();
            for (var i = 0; i < _disassembler.Blocks[BlockBox.SelectedIndex].Functions.Count; i++)
                FunctionBox.Items.Add($"Function {i}");
            FunctionBox.SelectedIndex = 0;
        }

        private void FunctionBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FunctionBox.SelectedIndex == -1)
                return;

            LoadFunction(BlockBox.SelectedIndex, FunctionBox.SelectedIndex);

            // Return tree view to top left position
            var scrollViewer = (ScrollViewer) DecompilerTree.Template.FindName("TreeScrollViewer", DecompilerTree);
            scrollViewer.ScrollToTop();
            scrollViewer.ScrollToLeftEnd();
        }

        private void LoadFunction(int blockId, int functionId)
        {
            LoadDisassembly(blockId, functionId);
            LoadDecompilation(blockId, functionId);
        }

        private void LoadDisassembly(int blockId, int functionId)
        {
            if (_disassembler == null)
                return;

            _asmList = new Dictionary<uint, FrameworkElement>();
            foreach (var instruction in _disassembler.Blocks[blockId].Functions[functionId].Instructions)
            {
                var instructionPanel = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Tag = instruction
                };

                var opcode = Opcode.Find(instruction.Value.Opcode);
                instructionPanel.Children.Add(new MnemonicElement(opcode));

                switch (opcode.Operand)
                {
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Value:
                        instructionPanel.Children.Add(new NumberElement(instruction.Value.Operand.ToString(), null));
                        break;
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Variable:
                        instructionPanel.Children.Add(new VariableElement(_disassembler.Blocks[blockId]
                            .Variables[instruction.Value.Operand], null));
                        break;
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Integer:
                        instructionPanel.Children.Add(new NumberElement(_disassembler.Blocks[blockId]
                            .Integers[instruction.Value.Operand].Value.ToString(), null));
                        break;
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Float:
                        instructionPanel.Children.Add(new NumberElement(_disassembler.Blocks[blockId]
                            .Floats[instruction.Value.Operand].Value.ToString(), null));
                        break;
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Label:
                        instructionPanel.Children.Add(new OperatorElement(_disassembler.Blocks[blockId]
                                                                              .Labels[instruction.Value.Operand].Offset
                                                                              .ToString("X4") +
                                                                          $"({_disassembler.Blocks[blockId].Labels[instruction.Value.Operand].Name})",
                            null));
                        break;
                    case AtelLib.ATEL.Disassembler.Enums.OperandType.Call:
                        var method = AtelDatabase.FindFunction(instruction.Value.Operand);
                        instructionPanel.Children.Add(new MethodNameElement(
                            method?.Name ?? $"unknown_{instruction.Value.Operand:X4}",
                            null));
                        break;
                }

                _asmList[instruction.Key] = instructionPanel;
            }
            
            VariableList.ItemsSource = new ObservableCollection<VariableConstant>(_disassembler.Blocks[BlockBox.SelectedIndex].Variables);
            DisassemblerList.ItemsSource = _asmList;
        }

        private void LoadDecompilation(int blockId, int functionId)
        {
            if (_decompiler == null)
                return;

            // Create the list that will represent our TreeView, and a list used for non-recursive lookup
            _decList = new List<TreeViewItem>();
            _tokens = new Dictionary<uint, FrameworkElement>();

            // Create root item to represent our function
            var rootItem = new ExtendedTreeViewItem
            {
                Header = $"Function {functionId}",
                IsExpanded = true
            };

            _decList.Add(rootItem);

            // Add child script tokens to the root function token
            var tokens = _decompiler.Blocks[blockId].Functions[functionId].Tokens;
            foreach (var token in tokens.OrderBy(d => d.Offset))
                AddToken(rootItem, token);

            DecompilerTree.ItemsSource = _decList;
        }

        private void AddToken(ItemsControl parentItem, IToken token)
        {
            var tokenItem = new ExtendedTreeViewItem
            {
                Header = token.ToFrameworkElement(),
                Tag = token
            };

            tokenItem.RequestBringIntoView += (sender, args) => args.Handled = true;

            parentItem.Items.Add(tokenItem);

            switch (token)
            {
                case If ifToken:
                    if (ifToken.NextElse != null)
                        AddToken(parentItem, ifToken.NextElse);
                    tokenItem.IsExpanded = true;
                    break;
                case Else elseToken:
                    if (elseToken.NextElse != null)
                        AddToken(parentItem, elseToken.NextElse);
                    tokenItem.IsExpanded = true;
                    break;
                case Switch switchToken:
                    var switchCases = switchToken.Cases.GroupBy(c => c.Offset).ToList();
                    foreach (var @case in switchCases)
                    {
                        // caseNode will contain the last case once the loop is completed
                        TreeViewItem caseNode = null;

                        foreach (var t in @case)
                        {
                            // Create a node to represent the case header
                            caseNode = new ExtendedTreeViewItem
                            {
                                Header = t.ToFrameworkElement(),
                                Tag = t
                            };

                            // Add case to tree
                            tokenItem.Items.Add(caseNode);
                        }

                        // This should probably never happen
                        if (caseNode == null)
                            throw new Exception("No case node to add children to!");

                        // Add all children to last case node
                        foreach (var item in @case.Last().Children)
                            AddToken(caseNode, item);

                        caseNode.IsExpanded = true;
                    }

                    tokenItem.IsExpanded = true;
                    break;
            }

            if (token is IBlockToken blockToken)
            {
                foreach (var child in blockToken.Children)
                    if (child != null)
                        AddToken(tokenItem, child);
                tokenItem.IsExpanded = true;
            }

            if (token is Label labelToken && _decompiler.Blocks[BlockBox.SelectedIndex].Functions[FunctionBox.SelectedIndex].IsIgnored(labelToken.SourceLabel))
                tokenItem.Visibility = Visibility.Collapsed;

            _tokens[token.Offset] = tokenItem;
        }

        private IToken FindToken(FrameworkElement element)
        {
            while (true)
            {
                if (element.Tag is IToken token)
                    return token;

                if (element.Parent == null)
                    return null;

                element = (FrameworkElement) element.Parent;
            }
        }

        private void DecompilerTree_OnPreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (!(e.OriginalSource is FrameworkElement control))
                return;

            var token = FindToken(control);
            if (token == null)
            {
                if (!(DecompilerTree.SelectedItem is TreeViewItem tvi))
                    return;

                if (!(tvi.Tag is IToken tviToken))
                    return;

                token = tviToken;
            }

            var asmItem = _asmList.FirstOrDefault(i => i.Key == token.Offset);
            if (asmItem.Value == null)
                return;

            DisassemblerList.SelectedItem = asmItem;
            DisassemblerList.ScrollIntoView(asmItem);
            DisassemblyExpander.IsExpanded = true;
        }

        private void DisassemblerList_OnPreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (!(DisassemblerList.SelectedItem is KeyValuePair<uint, FrameworkElement> dli))
                return;

            var token = _tokens.FirstOrDefault(t => t.Key == dli.Key);
            if (token.Value == null)
                return;
        }

        private void EditFunctionsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            new FunctionDatabaseWindow().ShowDialog();
        }

        private async void AnalyzeReanalyzeFunctionMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            OverlayPanel.Visibility = Visibility.Visible;

            var blockId = BlockBox.SelectedIndex;
            var funcId = FunctionBox.SelectedIndex;
            await Task.Run(async () =>
            {
                try
                {
                    SetOverlayText("Decompiling...");
                    _decompiler.Blocks[blockId].DecompileFunction(funcId);
                }
                catch (Exception ex)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        await this.ShowMessageAsync("Exception occurred", ex.Message);
                    });
                }
            });

            LoadFunction(BlockBox.SelectedIndex, FunctionBox.SelectedIndex);

            OverlayPanel.Visibility = Visibility.Collapsed;
        }

        private void FileSaveDisassembly_OnClick(object sender, RoutedEventArgs e)
        {
            if (_file == null)
                return;

            var openDialog = new SaveFileDialog
            {
                Title = "Save disassembly",
                Filter = "ATEL disassembly|*.txt|All files|*.*"
            };

            if (!openDialog.ShowDialog() ?? true)
                return;

            var blockId = BlockBox.SelectedIndex;
            var functionId = FunctionBox.SelectedIndex;

            using (var fs = File.Open(openDialog.FileName, FileMode.Create))
            using (var sw = new StreamWriter(fs))
                foreach (var instruction in _disassembler.Blocks[blockId].Functions[functionId].Instructions)
                {
                    var opcode = Opcode.Find(instruction.Value.Opcode);
                    var instructionString = new StringBuilder(opcode.Mnemonic);

                    switch (opcode.Operand)
                    {
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Value:
                            instructionString.Append(" " + instruction.Value.Operand);
                            break;
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Variable:
                            instructionString.Append(" " + _disassembler.Blocks[blockId]
                                                         .Variables[instruction.Value.Operand].Name);
                            break;
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Integer:
                            instructionString.Append(" " + _disassembler.Blocks[blockId]
                                                         .Integers[instruction.Value.Operand].Value);
                            break;
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Float:
                            instructionString.Append(" " + _disassembler.Blocks[blockId]
                                                         .Floats[instruction.Value.Operand].Value);
                            break;
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Label:
                            instructionString.Append(" " + _disassembler.Blocks[blockId]
                                                         .Labels[instruction.Value.Operand].Name);
                            instructionString.Append(
                                $"    // {_disassembler.Blocks[blockId].Labels[instruction.Value.Operand].Offset:X4}");
                            break;
                        case AtelLib.ATEL.Disassembler.Enums.OperandType.Call:
                            var method = AtelDatabase.FindFunction(instruction.Value.Operand);
                            instructionString.Append(" " + (method?.Name ?? $"unknown_{instruction.Value.Operand:X4}"));
                            break;
                    }

                    sw.WriteLine($"{instruction.Key:X4} {instructionString.ToString()}");
                }
        }

        private void AnalyzePreanalysisMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void DecompilerTree_OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!(e.OriginalSource is FrameworkElement element))
                return;

            if (!(element.Tag is IToken token))
                return;

            ContextMenu contextMenu = null;
            switch (token)
            {
                case Call callToken:
                    contextMenu = GetCallContextMenu(callToken);
                    break;
            }

            if (contextMenu == null)
                return;

            contextMenu.PlacementTarget = (FrameworkElement) e.OriginalSource;
            contextMenu.IsOpen = true;
        }

        private ContextMenu GetCallContextMenu(Call callToken)
        {
            var menu = new ContextMenu
            {
                Items =
                {
                    new MenuItem
                    {
                        Header = $"Function: {(callToken.Function?.Name ?? callToken.Name).Replace("_", "__")}",
                        IsEnabled = false
                    },
                    new Separator()
                }
            };
            if (callToken.IsUnknown)
            {
                menu.Items.Add(new MenuItem {Header = "Create definition"});
            }
            else
            {
                menu.Items.Add(new MenuItem {Header = "Edit definition"});
            }

            return menu;
        }

        private void DisassemblerList_OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (DisassemblerList.SelectedIndex == -1)
                return;

            var offset = (uint) DisassemblerList.SelectedValue;
            var instr = _disassembler.Blocks[BlockBox.SelectedIndex].Functions[FunctionBox.SelectedIndex]
                .Instructions[offset];
            var opcode = Opcode.Find(instr.Opcode);
            switch (opcode.Operand)
            {
                case AtelLib.ATEL.Disassembler.Enums.OperandType.Label:
                    var labelOffset = _disassembler.Blocks[BlockBox.SelectedIndex].Labels[instr.Operand].Offset;
                    var labelIndex = _asmList.ToList().FindIndex(i => i.Key == labelOffset);
                    if (labelIndex == -1)
                        break;
                    DisassemblerList.SelectedIndex = labelIndex;
                    DisassemblerList.ScrollIntoView(DisassemblerList.SelectedItem);
                    e.Handled = true;

                    break;
            }
        }

        private void EditLookupsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            new LookupEditorWindow().ShowDialog();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void VariableList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if (VariableList.SelectedItem == null)
                return;

            var selectedVariable = (VariableConstant)VariableList.SelectedItem;
            var newName = await this.ShowInputAsync("Enter name", "Enter a new name for the variable", new MetroDialogSettings
            {
                DefaultText = selectedVariable.Name
            });
            if (newName == null)
                return;
            selectedVariable.Name = newName;
            var binding = VariableList.GetBindingExpression(ItemsControl.ItemsSourceProperty);
            binding?.UpdateTarget();
        }

        private void FileOpenArchive_OnClick(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = "FFX/X-2 game data|FFX_Data.vbf;FFX2_Data.vbf|All files|*.*"
            };
            if (!ofd.ShowDialog() ?? true)
                return;

            var gfd = new GameDataDialog(ofd.FileName);
            gfd.ShowDialog();
            if (gfd.DialogResult == false)
                return;

            LoadBinary(gfd.MonsterFile);
        }
    }
}