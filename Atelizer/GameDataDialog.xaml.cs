﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FFXLib.FileSystem;
using FFXLib.FileSystem.Data;
using FFXLib.FileSystem.Data.Records;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using FFXLib.FileSystem.VBF;

namespace Atelizer
{
    /// <summary>
    /// Interaction logic for GameDataDialog.xaml
    /// </summary>
    public partial class GameDataDialog
    {
        private const string FfxMonster = "ffx_ps2/ffx/master/new_uspc/battle/kernel/monster{0}.bin";
        private const string FfxMonsterFile = "ffx_ps2/ffx/master/jppc/battle/mon/_m{0:D3}/m{0:D3}.bin";
        private const string Ffx2Monster = "ffx_ps2/ffx2/master/new_uspc/battle/kernel/monster{0}.bin";
        private VbfReader _vbfReader;
        public byte[] MonsterFile { get; private set; }

        public GameDataDialog(string gameDataFile)
        {
            InitializeComponent();
            _vbfReader = new VbfReader(gameDataFile);
            if (_vbfReader.Contains(string.Format(FfxMonster, 1)))
            {
                var encodingStream = _vbfReader.ExtractStream("ffx_ps2/ffx/master/jppc/ffx_encoding/ffxsjistbl_us.bin");
                var encodingReader = new BinaryReader(encodingStream, Encoding.UTF8);
                var encodingTable = new List<char>();
                while (encodingStream.Position < encodingStream.Length)
                    encodingTable.Add(encodingReader.ReadChar());
                Record.StringConverter = new StringConverter(encodingTable);

                var monsters = new List<Tuple<string, ushort>>();
                for (var i = 0; i < 3; i++)
                {
                    var file = _vbfReader.ExtractStream(string.Format(FfxMonster, i + 1));
                    var kf = new KernelFile<Monster>(file);
                    foreach (var monster in kf.Records)
                        monsters.Add(Tuple.Create(monster.Name, monster.MonsterId));
                }

                MonsterList.ItemsSource = monsters;
            }
            else if (_vbfReader.Contains(Ffx2Monster))
            {
            }
        }

        private void MonsterList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MonsterFile = _vbfReader.ExtractStream(string.Format(FfxMonsterFile, (ushort) MonsterList.SelectedValue)).ToArray();
            DialogResult = true;
            Close();
        }
    }
}