﻿using System.Collections.Generic;
using System.Windows.Controls;
using AtelLib.ATEL.Database;
using AtelLib.ATEL.Database.Classes;
using AtelLib.ATEL.Database.Enums;

namespace Atelizer
{
    /// <summary>
    /// Interaction logic for FunctionDatabaseWindow.xaml
    /// </summary>
    public partial class FunctionDatabaseWindow
    {
        private List<AtelFunction> _functions;
        public Dictionary<int, string> Lookups { get; set; }

        public FunctionDatabaseWindow()
        {
            InitializeComponent();
            Lookups = AtelDatabase.GetLookups();
            GameSelector.SelectedIndex = 0;
        }

        private void GameSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var game = (GameType)((ComboBoxItem) GameSelector.SelectedItem).Tag;
            FunctionList.ItemsSource = _functions = AtelDatabase.GetFunctions(game);
        }

        private void FunctionList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FunctionGrid.DataContext = FunctionList.SelectedItem;
        }
    }
}
