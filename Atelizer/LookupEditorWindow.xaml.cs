﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using AtelLib.ATEL.Database;
using AtelLib.ATEL.Database.Classes;
using MahApps.Metro.Controls.Dialogs;

namespace Atelizer
{
    /// <summary>
    /// Interaction logic for LookupEditorWindow.xaml
    /// </summary>
    public partial class LookupEditorWindow
    {
        private Dictionary<int, string> _lookups;
        private AtelLookup _selectedLookup;

        public LookupEditorWindow()
        {
            InitializeComponent();

            LookupList.ItemsSource = _lookups = AtelDatabase.GetLookups();
        }

        private async void NewLookupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var name = await this.ShowInputAsync("Create new lookup", "Please enter a name for the lookup");
            if (name == null)
                return;

            var lookup = new AtelLookup
            {
                Name = name,
                Items = new List<AtelLookupItem>()
            };

            AtelDatabase.AddLookup(lookup);
            LookupList.ItemsSource = _lookups = AtelDatabase.GetLookups();
        }

        private void LookupList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LookupList.SelectedValue == null)
            {
                ValuesGroup.DataContext = null;
                return;
            }
            
            _selectedLookup = AtelDatabase.GetLookup((int) LookupList.SelectedValue);
            ValuesGroup.DataContext = _selectedLookup;
        }

        private async void NewValueButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (LookupList.SelectedValue == null)
            {
                return;
            }

            var val = await this.ShowInputAsync("New integer value", "Please enter the value to add");
            if (val == null)
                return;
            
            if (int.TryParse(val, out var intVal))
            {
                var name = await this.ShowInputAsync("New integer value", "Please enter a name for the value");
                if (!Regex.IsMatch(name, "^[_a-zA-Z][_a-zA-Z0-9]*$"))
                {
                    await this.ShowMessageAsync("Invalid name",
                        "Name must start with a letter or underscore and contain only alphanumeric characters or underscores");
                    return;
                }

                AtelDatabase.AddValue(_selectedLookup, intVal, name);
                ValuesGroup.DataContext = null;
                ValuesGroup.DataContext = _selectedLookup;
            }
            else
            {
                await this.ShowMessageAsync("Invalid value", "Only integer values are allowed.");
            }
        }
    }
}