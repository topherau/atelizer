﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FFXLib.FileSystem.VBF.zlib.net;

namespace FFXLib.FileSystem.VBF
{
    public class VbfReader : IDisposable
    {
        private readonly string _filePath;
        private readonly FileStream _fileStream;
        private readonly BinaryReader _fileReader;

        private string[] _fileNames;
        private byte[][] _fileNameHashes;
        private ulong[] _fileNameOffsets;
        private ulong[] _startOffsets;
        private ulong[] _originalSizes;
        private uint[] _blockListStarts;
        private ushort[] _blockList;

        public VbfReader(string filePath)
        {
            _filePath = filePath;
            _fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            _fileReader = new BinaryReader(_fileStream);

            Read();
        }

        private void Read()
        {
            // Ensure the file starts with the appropriate bytes
            if ((int)_fileReader.ReadUInt32() != 1264144979)
                throw new Exception("Invalid header!");

            var headerLength = _fileReader.ReadUInt32();
            var numFiles = _fileReader.ReadUInt64();

            _fileNameHashes = new byte[numFiles][];
            _fileNameOffsets = new ulong[numFiles];

            _startOffsets = new ulong[numFiles];
            _originalSizes = new ulong[numFiles];
            _blockListStarts = new uint[numFiles];

            // Read MD5 hashes
            for (var i = 0UL; i < numFiles; i++)
                _fileNameHashes[i] = _fileReader.ReadBytes(16);

            // Read file information
            for (var i = 0UL; i < numFiles; i++)
            {
                _blockListStarts[i] = _fileReader.ReadUInt32();
                _fileReader.ReadUInt32(); // Unknown value, can be safely ignored
                _originalSizes[i] = _fileReader.ReadUInt64();
                _startOffsets[i] = _fileReader.ReadUInt64();
                _fileNameOffsets[i] = _fileReader.ReadUInt64();
            }

            // Read string table
            var stringTableLength = _fileReader.ReadUInt32();
            var stringTable = _fileReader.ReadBytes((int)stringTableLength - 4);
            _fileNames = Encoding.ASCII.GetString(stringTable).Trim('\0').Split('\0');
            if (_fileNames.Length != (int)numFiles)
                throw new Exception("String count mismatch.");

            // Calculate number of blocks in block list
            uint blockCount = 0;
            foreach (var originalSize in _originalSizes)
            {
                blockCount += (uint)(originalSize / 65536UL);
                if ((long)(originalSize % 65536UL) != 0L)
                    ++blockCount;
            }

            // Read block list
            _blockList = new ushort[blockCount];
            for (var i = 0U; i < blockCount; i++)
                _blockList[i] = _fileReader.ReadUInt16();

            // Read header into buffer
            _fileStream.Seek(0, SeekOrigin.Begin);
            var headerBytes = _fileReader.ReadBytes((int)headerLength);

            // Read hash from file (last 16 bytes)
            _fileStream.Seek(-16, SeekOrigin.End);
            var hashBytes = _fileReader.ReadBytes(16);

            var headerHash = MD5.Create().ComputeHash(headerBytes);
            if (!headerHash.SequenceEqual(hashBytes))
                throw new Exception("Unable to validate header hash.");
        }

        public MemoryStream ExtractStream(string filename)
        {
            var outputStream = new MemoryStream();
            var result = ExtractToStream(filename, outputStream);

            if (!result)
                return null;

            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        public bool ExtractToStream(string filename, Stream outputStream)
        {
            // Find index of file in archive
            var fileIndex = _fileNames.ToList()
                .FindIndex(f => string.Compare(f, filename, StringComparison.OrdinalIgnoreCase) == 0);
            if (fileIndex == -1)
                return false;

            // Calculate number of blocks to read from archive
            var originalSize = _originalSizes[fileIndex];
            var blockCount = (int)(originalSize / 65536);
            var blockRemainder = (int)(originalSize % 65536);
            if (blockRemainder != 0)
                blockCount++;
            else
                blockRemainder = 65536;

            var startOffset = _startOffsets[fileIndex];
            var blockListStart = _blockListStarts[fileIndex];


            using (var stream = File.Open(_filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // Seek to start location of block data in archive
                stream.Seek((long)startOffset, SeekOrigin.Begin);

                for (var i = 0; i < blockCount; i++)
                {
                    // Get block length from block list
                    var blockLength = (int)_blockList[blockListStart + i];

                    // Since the length value is stored as a ushort, a value of 0 means the block was 65536 bytes
                    if (blockLength == 0)
                        blockLength = 65536;

                    // Read compressed block into buffer
                    var compressedBytes = new byte[blockLength];
                    stream.Read(compressedBytes, 0, blockLength);

                    // All blocks except the very last one have an original length of 65536 bytes
                    var decompressedLength = i != blockCount - 1
                        ? 65536
                        : blockRemainder;

                    // Create a buffer for the decompressed data
                    byte[] decompressedBytes;

                    // If we are processing the final block, and the data is uncompressed, skip decompression
                    if (i == blockCount - 1)
                        if (blockLength == blockRemainder)
                            goto MoveUncompressedData;

                    // Compressed blocks have a length less than 65536 bytes
                    if (blockLength != 65536)
                    {
                        try
                        {
                            decompressedBytes = new byte[decompressedLength];

                            using (var defStream = new ZOutputStream(new MemoryStream(decompressedBytes)))
                                defStream.Write(compressedBytes, 0, blockLength);

                            goto WriteBuffer;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to extract file.", ex);
                        }
                    }

                    MoveUncompressedData:
                    decompressedBytes = compressedBytes;

                    WriteBuffer:
                    outputStream.Write(decompressedBytes, 0, decompressedLength);
                }
            }

            return true;
        }

        public bool Contains(string filename)
        {
            return _fileNames.Contains(filename);
        }

        public void Dispose()
        {
            _fileStream?.Close();
            _fileStream?.Dispose();
            _fileReader?.Dispose();
        }
    }
}